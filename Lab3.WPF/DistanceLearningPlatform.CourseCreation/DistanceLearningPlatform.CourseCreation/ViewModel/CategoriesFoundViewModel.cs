﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class CategoriesFoundViewModel : CourseEntitiesFoundViewModel<CourseCategoryViewModel>
    {
        private static readonly string ADMIN_ROLE = "ADMIN";
        private ObservableCollection<CourseCategoryViewModel> categoriesFound;
        private CourseOperationsViewModel courseOperationsViewModel;
        public CategoriesFoundViewModel() { }

        public CategoriesFoundViewModel(CourseOperationsViewModel courseOperationsViewModel)
        {
            this.courseOperationsViewModel = courseOperationsViewModel;
            this.categoriesFound = new ObservableCollection<CourseCategoryViewModel>();
        }

        public override ObservableCollection<CourseCategoryViewModel> EntitiesFound { get => categoriesFound; set { categoriesFound = value; OnPropertyChanged("EntitiesFound"); } }


        public override void FindCourseEntitiesWhereNameContains(string value)
        {
            EntitiesFound = courseOperationsViewModel.FindCategoriesWhereNameContains(value);
        }
        public void DeleteCategory(object args)
        {
            var currentUserRoleName = App.CourseOperationsViewModel.User.Role.Name;
            if(currentUserRoleName == ADMIN_ROLE)
            {
                CourseCategoryViewModel categoryViewModel = (CourseCategoryViewModel)SelectedItem;
                bool result = App.CourseOperationsViewModel.DeleteCategory(categoryViewModel);
                String message = result ? "Category deleted" : "Category WAS NOT deleted";
                MessageBox.Show(message);
            }
            else
            {
                MessageBox.Show("You don't have appropriate role for deleting this entity");
            }
           
        }
        public void UpdateCategory(object agrs)
        {
            var currentUserRoleName = App.CourseOperationsViewModel.User.Role.Name;
            if (currentUserRoleName == ADMIN_ROLE)
            {
                CourseCategoryViewModel categoryViewModel = (CourseCategoryViewModel)SelectedItem;
                CategoryOperationViewModel categoryOperationViewModel = new CategoryOperationViewModel(App.CourseOperationsViewModel, categoryViewModel);
                CategoryOperationWindow window = new CategoryOperationWindow(categoryOperationViewModel);
                window.Show();
            }
            else
            {
                MessageBox.Show("You don't have appropriate role for updating this entity");
            }
        }
    }
}
