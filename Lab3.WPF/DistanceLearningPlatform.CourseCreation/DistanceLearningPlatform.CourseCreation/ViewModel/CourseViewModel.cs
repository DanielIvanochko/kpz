﻿using DistanceLearningPlatform.CourseCreation.Model.Course;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class CourseViewModel : ViewModelBase
    {
        private String name;
        private String description;
        private CourseCategoryViewModel category;
        private SpecializationViewModel specialization;
        private ObservableCollection<PlatformMemberViewModel> members;
        public CourseViewModel()
        {
        }

        public CourseViewModel(string name, string description, CourseCategoryViewModel category, SpecializationViewModel specialization, ObservableCollection<PlatformMemberViewModel> members)
        {
            this.name = name;
            this.description = description;
            this.category = category;
            this.specialization = specialization;
            this.members = members;
        }

        public string Name { get => name; set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public string Description { get => description; set { description = value; OnPropertyChanged("Description"); }  }
        public CourseCategoryViewModel Category { get => category; set { category = value; OnPropertyChanged("Category"); } }
        public SpecializationViewModel Specialization { get => specialization; set { specialization = value; OnPropertyChanged("Specialization"); } }
        public ObservableCollection<PlatformMemberViewModel> Members { get => members; set { members = value; OnPropertyChanged("Members"); } }
    }
}
