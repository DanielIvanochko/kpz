﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class CoursesFoundViewModel : CourseEntitiesFoundViewModel<CourseViewModel>
    {
        private ObservableCollection<CourseViewModel> coursesFoundViewModel;
        private CourseOperationsViewModel courseOperationsViewModel;
        
        public CoursesFoundViewModel()
        {
            coursesFoundViewModel = new ObservableCollection<CourseViewModel>();
        }

        public CoursesFoundViewModel(CourseOperationsViewModel courseOperationsViewModel)
        {
            this.courseOperationsViewModel = courseOperationsViewModel;
            coursesFoundViewModel = new ObservableCollection<CourseViewModel>();
        }
        public override void FindCourseEntitiesWhereNameContains(string value)
        {
            EntitiesFound = courseOperationsViewModel.FindCoursesWhereNameContains(value);
        }

        public void DeleteCourseCommand(object atrs)
        {
            var courseViewModel = (CourseViewModel)SelectedItem;
            bool result = App.CourseOperationsViewModel.DeleteCourse(courseViewModel);
            String message = result ? "Course deleted" : "Course WAS NOT deleted";
            MessageBox.Show(message);
        }

        public void UpdateCourseCommand(object agrs)
        {
            var courseViewModel = (CourseViewModel)SelectedItem;
            CourseOperationViewModel courseOperationViewModel = new CourseOperationViewModel(courseViewModel, App.CourseOperationsViewModel);
            CourseOperationWindow courseOperationWindow = new CourseOperationWindow(courseOperationViewModel);
            courseOperationWindow.Show();
        }

        public override ObservableCollection<CourseViewModel> EntitiesFound { get => coursesFoundViewModel; 
            set { coursesFoundViewModel = value; OnPropertyChanged("EntitiesFound"); } }
    }
}
