﻿using DistanceLearningPlatform.CourseCreation.Model;
using DistanceLearningPlatform.CourseCreation.Model.Course;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class CourseOperationsViewModel : ViewModelBase
    {
        private UserViewModel user;
        private PlatformMemberViewModel member;
        private ObservableCollection<CourseViewModel> courses;
        private ObservableCollection<GroupViewModel> groups;
        private ObservableCollection<SpecializationViewModel> specializations;
        private ObservableCollection<PlatformMemberViewModel> platformMembers;
        private ObservableCollection<RoleViewModel> roles;
        private ObservableCollection<CourseCategoryViewModel> categories;
        private ICommand deleteCommand;
        private ICommand updateCommand;
        public CourseOperationsViewModel() { }

        public CourseOperationsViewModel(UserViewModel user, 
            PlatformMemberViewModel member, 
            ObservableCollection<CourseViewModel> courses, 
            ObservableCollection<GroupViewModel> groups,
            ObservableCollection<SpecializationViewModel> specializations,
            ObservableCollection<PlatformMemberViewModel> courseMembers, 
            ObservableCollection<RoleViewModel> roles,
            ObservableCollection<CourseCategoryViewModel> categories)
        {
            this.user = user;
            this.member = member;
            this.courses = courses;
            this.groups = groups;
            this.specializations = specializations;
            this.platformMembers = courseMembers;
            this.roles = roles;
            this.categories = categories;
        }

        public UserViewModel User { get => user; set { 
                user = value; OnPropertyChanged("User");
            } }
        public PlatformMemberViewModel Member { get => member; set { member = value; OnPropertyChanged("Member"); } }
        public ObservableCollection<CourseViewModel> Courses { get => courses; set { courses = value; OnPropertyChanged("Courses"); } }
        public ObservableCollection<GroupViewModel> Groups { get => groups; set { groups = value; OnPropertyChanged("Groups"); } }
        public ObservableCollection<SpecializationViewModel> Specializations { get {
                return App.Mapper.Map<ObservableCollection<SpecializationViewModel>>(App.CourseOperationsModel.Specializations);
            }
            set { specializations = value; OnPropertyChanged("Specializarions"); } }
        public ObservableCollection<PlatformMemberViewModel> PlatformMembers { get => platformMembers; set { platformMembers = value; OnPropertyChanged("PlatformMembers"); } }
        public ObservableCollection<RoleViewModel> Roles { get => roles; set => roles = value; }
        public ObservableCollection<CourseCategoryViewModel> Categories { get
            {
                return App.Mapper.Map<ObservableCollection<CourseCategoryViewModel>>(App.CourseOperationsModel.Categories);
            } set => categories = value; }
        public ICommand DeleteCommand { get => deleteCommand; set
            {
                deleteCommand = value;
                OnPropertyChanged("DeleteCommand");
            } }

        public ICommand UpdateCommand { get => updateCommand; set { updateCommand = value; OnPropertyChanged("UpdateCommand"); } }

        public bool AddCourse(CourseViewModel courseViewModel)
        {
            Course course = App.Mapper.Map<CourseViewModel, Course>(courseViewModel);
            bool result = App.CourseOperationsModel.AddCourse(course);
            return result;
        }

        public bool AddGroup(GroupViewModel groupViewModel)
        {
            Group group = App.Mapper.Map<GroupViewModel, Group>(groupViewModel);
            bool result = App.CourseOperationsModel.AddGroup(group);
            return result;
        }
        public bool AddMember(PlatformMemberViewModel platformMemberViewModel)
        {
            PlatformMember platformMember = App.Mapper.Map<PlatformMemberViewModel, PlatformMember>(platformMemberViewModel);
            bool result = App.CourseOperationsModel.AddMember(platformMember);
            return result;
        }

        public ObservableCollection<CourseViewModel> FindCoursesWhereNameContains(String value)
        {
            var coursesFound = App.CourseOperationsModel.FindCoursesWhereNameContains(value);
            return App.Mapper.Map<ObservableCollection<CourseViewModel>>(coursesFound);
        }
        public ObservableCollection<CourseCategoryViewModel> FindCategoriesWhereNameContains(String value)
        {
            var categoriesFound = App.CourseOperationsModel.FindCategoriesWhereNameContains(value);
            return App.Mapper.Map<ObservableCollection<CourseCategoryViewModel>>(categoriesFound);
        }
        public ObservableCollection<SpecializationViewModel> FindSpecializationsWhereNameContains(String value)
        {
            var specsFound = App.CourseOperationsModel.FindSpecializationsWhereNameContains(value);
            return App.Mapper.Map<ObservableCollection<SpecializationViewModel>>(specsFound);
        }
        public ObservableCollection<PlatformMemberViewModel> FindPlatformMembersWhereNameContains(String value)
        {
            var membersFound = App.CourseOperationsModel.FindPlatformMembersWhereNameContains(value);
            return App.Mapper.Map<ObservableCollection<PlatformMemberViewModel>>(membersFound);
        }
        public ObservableCollection<GroupViewModel> FindGroupsWhereNameContains(String value)
        {
            var groupsFound = App.CourseOperationsModel.FindGroupsWhereNameContains(value);
            return App.Mapper.Map<ObservableCollection<GroupViewModel>>(groupsFound);
        }
        public bool DeleteCourse(CourseViewModel courseViewModel)
        {
            Course course = App.Mapper.Map<Course>(courseViewModel);
            bool result = App.CourseOperationsModel.DeleteCourse(course);
            return result;
        }
        public bool DeleteCategory(CourseCategoryViewModel categoryViewModel)
        {
            CourseCategory courseCategory = App.Mapper.Map<CourseCategory>(categoryViewModel);
            bool result = App.CourseOperationsModel.DeleteCategory(courseCategory);
            return result;
        }
        public bool DeleteSpecialization(SpecializationViewModel specializationViewModel)
        {
            Specialization specialization = App.Mapper.Map<Specialization>(specializationViewModel);
            bool result = App.CourseOperationsModel.DeleteSpecialization(specialization);
            return result;
        }
        public bool DeleteMember(PlatformMemberViewModel platformMemberViewModel)
        {
            var member = App.Mapper.Map<PlatformMember>(platformMemberViewModel);
            bool result = App.CourseOperationsModel.DeleteMember(member);
            return result;
        }
        public bool DeleteGroup(GroupViewModel groupViewModel)
        {
            var group = App.Mapper.Map<Group>(groupViewModel);
            bool result = App.CourseOperationsModel.DeleteGroup(group);
            return result;
        }
        public bool UpdateCourse(CourseViewModel courseViewModel, String oldCourseName)
        {
            var course = App.Mapper.Map<Course>(courseViewModel);
            return App.CourseOperationsModel.UpdateCourse(oldCourseName, course);
        }
        public bool UpdateGroup(GroupViewModel groupViewModel, String oldGroupName)
        {
            var group = App.Mapper.Map<Group>(groupViewModel);
            return App.CourseOperationsModel.UpdateGroup(oldGroupName, group);
        }
        public bool UpdateMember(PlatformMemberViewModel memberViewModel, String oldName)
        {
            var member = App.Mapper.Map<PlatformMember>(memberViewModel);
            return App.CourseOperationsModel.UpdateMember(member, oldName);
        }
        public bool AddCategory(CourseCategoryViewModel categoryViewModel)
        {
            var category = App.Mapper.Map<CourseCategory>(categoryViewModel);
            return App.CourseOperationsModel.AddCategory(category);
        }
        public bool UpdateCategory(CourseCategoryViewModel categoryViewModel, String oldName)
        {
            var category = App.Mapper.Map<CourseCategory>(categoryViewModel);
            return App.CourseOperationsModel.UpdateCategory(category, oldName);
        }
        public bool AddSpecialization(SpecializationViewModel specializationViewModel)
        {
            var specialization = App.Mapper.Map<Specialization>(specializationViewModel);
            return App.CourseOperationsModel.AddSpecialization(specialization);
        }
        public bool UpdateSpecialization(SpecializationViewModel specializationViewModel, String oldName)
        {
            var specialization = App.Mapper.Map<Specialization>(specializationViewModel);
            return App.CourseOperationsModel.UpdateSpecialization(specialization, oldName);
        }
    }
}
