﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class GroupOperationViewModel : ViewModelBase
    {
        private string windowTitle;
        private string groupName;
        private ObservableCollection<PlatformMemberViewModel> members;
        private ObservableCollection<SpecializationViewModel> specializations;
        private int selectedSpecializationIndex;
        private ICommand groupOperationCommand;
        private ICommand addMemberCommand;
        private ICommand deleteMemberCommand;
        private String memberFirstName;
        private String memberLastName;
        private PlatformMemberViewModel selectedMember;
        private CourseOperationsViewModel courseOperationsViewModel;
        private String oldName;
        public GroupOperationViewModel(CourseOperationsViewModel courseOperationsViewModel)
        {
            windowTitle = "Create Group";
            groupName = "";
            members = new ObservableCollection<PlatformMemberViewModel>();
            addMemberCommand= new Command.Command(AddMember);
            deleteMemberCommand = new Command.Command(DeleteMember);
            this.courseOperationsViewModel = courseOperationsViewModel;
            groupOperationCommand = new Command.Command(AddGroup);
            specializations = courseOperationsViewModel.Specializations;
            selectedSpecializationIndex = 0;
        }
        public GroupOperationViewModel(GroupViewModel groupViewModel, CourseOperationsViewModel courseOperationsViewModel)
        {
            windowTitle = "Update Group";
            groupName = groupViewModel.Name;
            members = groupViewModel.Members;
            addMemberCommand = new Command.Command(AddMember);
            deleteMemberCommand = new Command.Command(DeleteMember);
            this.courseOperationsViewModel = courseOperationsViewModel;
            oldName = groupViewModel.Name;
            groupOperationCommand = new Command.Command(UpdateGroup);
            specializations = courseOperationsViewModel.Specializations;
            findSelectedSpecializationIndex(groupViewModel, specializations);
        }
        private void findSelectedSpecializationIndex(GroupViewModel groupViewModel, ObservableCollection<SpecializationViewModel> specializations)
        {
            var spec = specializations.FirstOrDefault(s => s.Name == groupViewModel.Specialization.Name);
            if (spec != null)
            {
                for (int i = 0; i < specializations.Count(); i++)
                {
                    if (specializations[i].Name == spec.Name)
                    {
                        SelectedSpecializationIndex = i;
                        break;
                    }
                }
            }
            else
            {
                SelectedSpecializationIndex = 0;
            }
        }

        public string WindowTitle { get => windowTitle; set { windowTitle = value; OnPropertyChanged("WindowTitle"); } }
        public string GroupName { get => groupName; set { groupName = value; } }
        public ObservableCollection<PlatformMemberViewModel> Members { get => members; set { members = value; OnPropertyChanged("Members"); } }
        public ICommand GroupOperationCommand { get => groupOperationCommand; set { groupOperationCommand = value; OnPropertyChanged("GroupOperationCommand"); } }
        public ICommand AddMemberCommand { get => addMemberCommand; set { addMemberCommand = value; OnPropertyChanged("AddMemberCommand"); } }

        public string MemberFirstName { get => memberFirstName; set { memberFirstName = value; OnPropertyChanged("MemberFirstName"); } }
        public string MemberLastName { get => memberLastName; set { memberLastName = value; OnPropertyChanged("MemberLastName"); } }

        public PlatformMemberViewModel SelectedMember { get => selectedMember; set { selectedMember = value; OnPropertyChanged("SelectedMember"); } }

        public ICommand DeleteMemberCommand { get => deleteMemberCommand; set { deleteMemberCommand = value; OnPropertyChanged("DeleteMemberComand"); } }

        public string OldName { get => oldName; set => oldName = value; }

        public int SelectedSpecializationIndex { get => selectedSpecializationIndex; set { selectedSpecializationIndex = value; OnPropertyChanged("SelectedSpecializationIndex"); } }

        public ObservableCollection<SpecializationViewModel> Specializations { get => specializations; set { specializations = value; OnPropertyChanged("Specializations"); } }

        public void AddMember(object args)
        {
            PlatformMemberViewModel member = new PlatformMemberViewModel(memberFirstName, memberLastName);
            Members.Add(member);
            MemberFirstName = "";
            MemberLastName = "";
        }
        public void DeleteMember(object args)
        {
            if (SelectedMember != null)
            {
                Members.Remove(SelectedMember);
            }
        }
        public void AddGroup(object args)
        {
            var specialization = specializations[SelectedSpecializationIndex];
            GroupViewModel groupViewModel = new GroupViewModel(GroupName, Members, specialization);
            bool result = courseOperationsViewModel.AddGroup(groupViewModel);
            String message = result ? "Group created" : "Group WAS NOT created";
            MessageBox.Show(message);
        }
        public void UpdateGroup(object args)
        {
            var specialization = specializations[SelectedSpecializationIndex];
            GroupViewModel groupViewModel = new GroupViewModel(GroupName, Members, specialization);
            bool result = courseOperationsViewModel.UpdateGroup(groupViewModel, oldName);
            String message = result ? "Group updated" : "Group WAS NOT updated";
            MessageBox.Show(message);
        }
    }
}
