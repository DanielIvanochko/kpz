﻿using DistanceLearningPlatform.CourseCreation.Model.Course;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

//PlatformMembersFoundViewModel
namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class PlatformMembersFoundViewModel : CourseEntitiesFoundViewModel<PlatformMemberViewModel>
    {
        private ObservableCollection<PlatformMemberViewModel> membersFoundViewModels;
        private CourseOperationsViewModel courseOperationsViewModel;
        public PlatformMembersFoundViewModel()
        {
            courseOperationsViewModel = App.CourseOperationsViewModel;
            membersFoundViewModels = new ObservableCollection<PlatformMemberViewModel>();
        }

        public PlatformMembersFoundViewModel(CourseOperationsViewModel courseOperationsViewModel)
        {
            this.courseOperationsViewModel = courseOperationsViewModel;
            membersFoundViewModels = new ObservableCollection<PlatformMemberViewModel>();
        }
        public override void FindCourseEntitiesWhereNameContains(string value)
        {
            EntitiesFound = courseOperationsViewModel.FindPlatformMembersWhereNameContains(value);
        }

        public override ObservableCollection<PlatformMemberViewModel> EntitiesFound { get => membersFoundViewModels; set { membersFoundViewModels = value; OnPropertyChanged("EntitiesFound"); } }

        public void DeletePlatformMember(object args)
        {
            var member = (PlatformMemberViewModel)SelectedItem;
            bool result = courseOperationsViewModel.DeleteMember(member);
            String message = result ? "Member deleted" : "Member WAS NOT deleted";
            MessageBox.Show(message);
        }
        public void UpdatePlatformMember(object args)
        {
            var member = (PlatformMemberViewModel)SelectedItem;
            PlatformMemberOperationViewModel platformMemberOperationView = new PlatformMemberOperationViewModel(courseOperationsViewModel, member);
            PlatformMemberOperationWindow window = new PlatformMemberOperationWindow(platformMemberOperationView);
            window.Show();
        }
    }
}
