﻿using DistanceLearningPlatform.CourseCreation.Converters;
using DistanceLearningPlatform.CourseCreation.Model.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        private ICommand openCourseOperationWindowCommand;
        private ICommand openFinderWindowCommand;
        private ICommand openGroupOperationWindowCommand;
        private ICommand openPlatformMemberOperationWindow;
        private ICommand openCategoryOperationWindow;
        private ICommand openSpecializationOperationWindow;
        private UserViewModel user;
        private RoleViewModel role;
        private IValueConverter roleConvertor;
        private static readonly string ADMIN_ROLE = "ADMIN";
        public MainWindowViewModel() {
            openCourseOperationWindowCommand = new Command.Command(OpenCourseCreationWindow);
            openFinderWindowCommand = new Command.Command(OpenFindWindow);
            openGroupOperationWindowCommand = new Command.Command(OpenGroupOperationWindow);
            openPlatformMemberOperationWindow = new Command.Command(OpenPlatformMemberOperationWindow);
            openCategoryOperationWindow = new Command.Command(OpenCategoryOperationWindow);
            openSpecializationOperationWindow = new Command.Command(OpenSpecializationOperationWindow);
            user = App.Mapper.Map<UserViewModel>(App.CourseOperationsModel.User);
            role = user.Role;
            roleConvertor = new UserRoleValueConverter();
        }
        
        public RoleViewModel Role { get => role; set { role = value; OnPropertyChanged("Role"); } }
        
        public ICommand OpenCourseOperationWindowCommand { get => openCourseOperationWindowCommand; 
            set { openCourseOperationWindowCommand = value; OnPropertyChanged("OpenCourseOperationWindowCommand"); } }
        public ICommand OpenFinderWindowCommand { get => openFinderWindowCommand; 
            set { openFinderWindowCommand = value; OnPropertyChanged("OpenFinderWindowCommand"); } }

        public ICommand OpenGroupOperationWindowCommand { get => openGroupOperationWindowCommand; 
            set { openGroupOperationWindowCommand = value; OnPropertyChanged("OpenGroupOperationWindowCommand"); } }

        public ICommand OpenPlatformMemberOperationWindowCommand { get => openPlatformMemberOperationWindow; set { openPlatformMemberOperationWindow = value; OnPropertyChanged("OpenPlatformMemberOperationWindowCommand"); } }
        public ICommand OpenCategoryOperationWindowCommand { get => openCategoryOperationWindow; set { openCategoryOperationWindow = value; OnPropertyChanged("OpenCategoryOperationWindowCommand"); } }
        public ICommand OpenSpecializationOperationWindowCommand { get => openSpecializationOperationWindow; set { openSpecializationOperationWindow = value; OnPropertyChanged("OpenSpecializationOperationWindowCommand"); } }

        public IValueConverter RoleConvertor { get => roleConvertor; set { roleConvertor = value; OnPropertyChanged("RoleConvertor"); } }

        private void OpenCourseCreationWindow(object args)
        {
            CourseOperationWindow courseOperation = new CourseOperationWindow();
            courseOperation.Show();
        }

        private void OpenFindWindow(object args)
        {
            CourseEntityFindWindow courseEntityFind = new CourseEntityFindWindow();
            courseEntityFind.Show();
        }
        private void OpenGroupOperationWindow(object args)
        {
            GroupOperationWindow groupOperationWindow = new GroupOperationWindow();
            groupOperationWindow.Show();
        }
        private void OpenPlatformMemberOperationWindow(object sender)
        {
            PlatformMemberOperationWindow platformMemberOperationWindow = new PlatformMemberOperationWindow();
            platformMemberOperationWindow.Show();
        }

        private void OpenCategoryOperationWindow(object sender)
        {
            if (role.Name != ADMIN_ROLE)
            {
                MessageBox.Show("You don't have appropriate role for using that command");
            }
            else
            {
                CategoryOperationWindow categoryOperationWindow = new CategoryOperationWindow();
                categoryOperationWindow.Show();
            }
        }

        private void OpenSpecializationOperationWindow(object sender)
        {
            SpecializationOperationWindow window = new SpecializationOperationWindow();
            window.Show();
        }
    }
}
