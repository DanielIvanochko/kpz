﻿using DistanceLearningPlatform.CourseCreation.Model.Course;
using DistanceLearningPlatform.CourseCreation.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DistanceLearningPlatform.CourseCreation.Util
{
    public class CourseEntityFindingMediator
    {
        public static void FindEntity(CourseEntityFindWindow window, String name, String courseEntityType)
        {
            if (courseEntityType == "Course")
            {
                CourseEntitiesFoundViewModel<CourseViewModel> courseEntitiesFoundViewModel = new CoursesFoundViewModel(App.CourseOperationsViewModel);
                window.ResultGrid.DataContext = courseEntitiesFoundViewModel;
                courseEntitiesFoundViewModel.FindCourseEntitiesWhereNameContains(name);
            }
            else if (courseEntityType == "Category")
            {
                CourseEntitiesFoundViewModel<CourseCategoryViewModel> courseEntitiesFoundViewModel = new CategoriesFoundViewModel(App.CourseOperationsViewModel);
                window.ResultGrid.DataContext = courseEntitiesFoundViewModel;
                courseEntitiesFoundViewModel.FindCourseEntitiesWhereNameContains(name);
            }
            else if (courseEntityType == "Specialization")
            {
                CourseEntitiesFoundViewModel<SpecializationViewModel> courseEntitiesFoundViewModel = new SpecializationsFoundViewModel(App.CourseOperationsViewModel);
                window.ResultGrid.DataContext = courseEntitiesFoundViewModel;
                courseEntitiesFoundViewModel.FindCourseEntitiesWhereNameContains(name);
            }
            else if (courseEntityType == "Platform member")
            {
                CourseEntitiesFoundViewModel<PlatformMemberViewModel> courseEntitiesFoundViewModel = new PlatformMembersFoundViewModel(App.CourseOperationsViewModel);
                window.ResultGrid.DataContext = courseEntitiesFoundViewModel;
                courseEntitiesFoundViewModel.FindCourseEntitiesWhereNameContains(name);
            }
            else if (courseEntityType == "Group")
            {
                CourseEntitiesFoundViewModel<GroupViewModel> courseEntitiesFoundViewModel = new GroupsFoundViewModel(App.CourseOperationsViewModel);
                window.ResultGrid.DataContext = courseEntitiesFoundViewModel;
                courseEntitiesFoundViewModel.FindCourseEntitiesWhereNameContains(name);
            }
        }
    }
}
