﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.Util
{
    public interface CourseEntityDeletion
    {
        bool DeleteCourseEntity();
    }
}
