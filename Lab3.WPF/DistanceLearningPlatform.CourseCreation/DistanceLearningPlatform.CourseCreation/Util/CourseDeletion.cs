﻿using DistanceLearningPlatform.CourseCreation.Model.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DistanceLearningPlatform.CourseCreation.Util
{
    public class CourseDeletion : CourseEntityDeletion
    {
        private List<Course> courses;
        private Course courseForDeletion;
        public CourseDeletion(List<Course> courses, Course courseForDeletion)
        {
            this.courses = courses;
            this.courseForDeletion = courseForDeletion;
        }

        public bool DeleteCourseEntity()
        {
            bool result = false;
            Course courseFound = courses.FirstOrDefault(crs => crs.Name == courseForDeletion.Name);
            if (courseFound != null)
            {
                result = courses.Remove(courseFound);
            }
            return result;
        }
    }
}
