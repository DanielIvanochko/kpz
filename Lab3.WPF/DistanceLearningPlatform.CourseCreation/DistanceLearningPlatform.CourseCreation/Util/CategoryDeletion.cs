﻿using DistanceLearningPlatform.CourseCreation.Model.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.Util
{
    public class CategoryDeletion : CourseEntityDeletion
    {
        private List<CourseCategory> categories;
        private CourseCategory categoryForDeletion;
        public CategoryDeletion(List<CourseCategory> categories, CourseCategory categoryForDeletion)
        {
            this.categories = categories;
            this.categoryForDeletion = categoryForDeletion;
        }

        public bool DeleteCourseEntity()
        {
            bool result = false;
            CourseCategory category = categories.FirstOrDefault(crs => crs.Name == categoryForDeletion.Name);
            if (category != null)
            {
                result = categories.Remove(category);
            }
            return result;
        }
    }
}
