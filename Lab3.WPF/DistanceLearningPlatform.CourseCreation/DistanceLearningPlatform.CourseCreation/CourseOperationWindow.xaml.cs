﻿using DistanceLearningPlatform.CourseCreation.Model;
using DistanceLearningPlatform.CourseCreation.Model.Course;
using DistanceLearningPlatform.CourseCreation.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DistanceLearningPlatform.CourseCreation
{
    /// <summary>
    /// Interaction logic for CourseOperation.xaml
    /// </summary>
    public partial class CourseOperationWindow : Window
    {
        private CourseOperationsViewModel courseOperationsViewModel;
        private CourseOperationViewModel courseOperationViewModel;
        public CourseOperationWindow()
        {
            InitializeComponent();
            courseOperationsViewModel = App.CourseOperationsViewModel;
            createCourseOperationViewModelFromMainViewModel();
            this.DataContext = courseOperationViewModel;
        }
        public CourseOperationWindow(CourseOperationViewModel courseOperationViewModel)
        {
            InitializeComponent();
            this.courseOperationViewModel = courseOperationViewModel;
            this.DataContext = courseOperationViewModel;
        }

        private void createCourseOperationViewModelFromMainViewModel()
        {
            var categories = courseOperationsViewModel.Categories;
            var specializations = courseOperationsViewModel.Specializations;
            var members = new ObservableCollection<PlatformMemberViewModel>();
            courseOperationViewModel = new CourseOperationViewModel(categories, specializations, members);
        }
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
