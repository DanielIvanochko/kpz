﻿using DistanceLearningPlatform.CourseCreation.Util;
using DistanceLearningPlatform.CourseCreation.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DistanceLearningPlatform.CourseCreation
{
    /// <summary>
    /// Interaction logic for CourseEntityFind.xaml
    /// </summary>
    public partial class CourseEntityFindWindow : Window
    {
        public CourseEntityFindWindow()
        {
            InitializeComponent();
            this.DataContext = App.CourseOperationsViewModel;
        }

        private void FindCourseEntity_Click(object sender, RoutedEventArgs e)
        {
            String name = this.EntitySearchName.Text;
            String courseEntityType = this.CourseEntityType.SelectionBoxItem.ToString();
            CourseEntityFindingMediator.FindEntity(this, name, courseEntityType);
        }

        private void Course_Select(object sender, RoutedEventArgs e)
        {
            if(this.ResultGrid!= null)
            {
                App.CourseOperationsViewModel.DeleteCommand = new Command.Command(new CoursesFoundViewModel().DeleteCourseCommand);
                App.CourseOperationsViewModel.UpdateCommand = new Command.Command(new CoursesFoundViewModel().UpdateCourseCommand);
            }
        }

        private void Category_Select(object sender, RoutedEventArgs e)
        {
            if (this.ResultGrid != null)
            {
                App.CourseOperationsViewModel.DeleteCommand = new Command.Command(new CategoriesFoundViewModel().DeleteCategory);
                App.CourseOperationsViewModel.UpdateCommand = new Command.Command(new CategoriesFoundViewModel().UpdateCategory);
            }
        }

        private void Specialization_Select(object sender, RoutedEventArgs e)
        {
            if (this.ResultGrid != null)
            {
                App.CourseOperationsViewModel.DeleteCommand = new Command.Command(new SpecializationsFoundViewModel().DeleteSpecialization);
                App.CourseOperationsViewModel.UpdateCommand = new Command.Command(new SpecializationsFoundViewModel().UpdateSpecialization);
            }
        }

        private void Member_Select(object sender, RoutedEventArgs e)
        {
            if (this.ResultGrid != null)
            {
                App.CourseOperationsViewModel.DeleteCommand = new Command.Command(new PlatformMembersFoundViewModel().DeletePlatformMember);
                App.CourseOperationsViewModel.UpdateCommand = new Command.Command(new PlatformMembersFoundViewModel().UpdatePlatformMember);
            }
        }

        private void Group_Select(object sender, RoutedEventArgs e)
        {
            if (this.ResultGrid != null)
            {
                App.CourseOperationsViewModel.DeleteCommand = new Command.Command(new GroupsFoundViewModel().DeleteGroup);
                App.CourseOperationsViewModel.UpdateCommand = new Command.Command(new GroupsFoundViewModel().UpdateGroup);
            }
        }
    }
}
