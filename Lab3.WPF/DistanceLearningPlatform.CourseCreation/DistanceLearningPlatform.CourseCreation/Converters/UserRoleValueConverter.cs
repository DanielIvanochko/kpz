﻿using DistanceLearningPlatform.CourseCreation.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace DistanceLearningPlatform.CourseCreation.Converters
{
    public class UserRoleValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            RoleViewModel roleViewModel = (RoleViewModel)value;
            string path = roleViewModel.Name == "ADMIN" ? "admin.png" : "user.png";
            Uri uri = new Uri(String.Format("C:\\University\\Projects\\Labs\\KPZ\\kpz\\Lab2.WPF\\DistanceLearningPlatform.CourseCreation\\DistanceLearningPlatform.CourseCreation\\Image\\User\\{0}", path), UriKind.Absolute);
            var resultImage = new BitmapImage(uri);
            return resultImage;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
