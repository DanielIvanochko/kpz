﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistanceLearningPlatform.CourseCreation.Model.Course;

namespace DistanceLearningPlatform.CourseCreation.DBService.Util
{
    public class AddSpecializationOperation : AddEntityOperation
    {
        private CourseCreationDatabaseEntities context;
        private Specialization specialization;

        public AddSpecializationOperation(Specialization specialization)
        {
            this.specialization = specialization;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool addEntity()
        {
            bool result = false;
            if (context != null && specialization != null)
            {
                bool doesSpecExist = context.Specializations.FirstOrDefault(s => s.name == specialization.Name) != null;
                if (!doesSpecExist)
                {
                    Specializations specializations = new Specializations() { name = specialization.Name };
                    context.Specializations.Add(specializations);
                    context.SaveChanges();
                    result = true;
                }
            }
            return result;
        }
    }
}
