﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistanceLearningPlatform.CourseCreation.Model.Course;
using System.Data.SqlClient;
using System.Xml;

namespace DistanceLearningPlatform.CourseCreation.DBService.Util
{
    public class AddMemberOperation : AddEntityOperation
    {
        private static readonly string connectionString = "data source=DESKTOP-GM1F63N\\SQLEXPRESS;initial catalog=CourseCreationDatabase;integrated security=True;";
        private CourseCreationDatabaseEntities context;
        private PlatformMember member;

        public AddMemberOperation(PlatformMember member)
        {
            this.member = member;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context=value; }
        public PlatformMember ModelEntity { get => member; set => member=value; }

        public bool addEntity()
        {
            /* bool result = false;
             if(context != null && member != null)
             {
                 Console.WriteLine("Checking whether member exists");
                 string fullname = member.LastName + member.FirstName;
                 bool doesMemberAlreadyExist = context.PlatformMembers
                     .FirstOrDefault(m => (m.lastname + m.firstname) == fullname) != null;
                 Console.WriteLine("Got member..");
                 if (!doesMemberAlreadyExist)
                 {
                     Console.WriteLine("Saving to DB...");
                     context.PlatformMembers
                         .Add(new PlatformMembers() { firstname = member.FirstName, lastname = member.LastName });
                     context.SaveChanges();
                     result = true;
                     Console.WriteLine("Saved!");
                 }
             }
             return result;*/

            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            bool result = false;
            if(!doesMemberExist(member, connection))
            {
                result = addMember(member, connection);
                connection.Close();
            }
            return result;
        }
        private bool addMember(PlatformMember member, SqlConnection connection)
        {
            bool result = false;
            string query = "insert into platformmembers(firstname, lastname) values ('"+member.FirstName+"','"+
                member.LastName+"');";
            SqlCommand command = new SqlCommand(query, connection);
            int number = command.ExecuteNonQuery();
            if (number == 1)
            {
                result = true;
                Console.WriteLine("Added platform member");
            }
            return result;
        }
        private bool doesMemberExist(PlatformMember member, SqlConnection connection)
        {
            string query = "select * from PlatformMembers where firstname='" + member.FirstName + "' and lastname='"
                + member.LastName + "';";
            SqlCommand selectMemberCommand = new SqlCommand(query, connection);
            bool result = false;
            try
            {
                SqlDataReader reader = selectMemberCommand.ExecuteReader();
                
                if (reader.Read())
                {
                    result = true;
                }
                reader.Close();
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
                result = true;
            }
            return result;
        }
    }
}
