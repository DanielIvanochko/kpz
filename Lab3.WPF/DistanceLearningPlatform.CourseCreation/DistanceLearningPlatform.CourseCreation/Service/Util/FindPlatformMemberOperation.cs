﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistanceLearningPlatform.CourseCreation.Model.Course;

namespace DistanceLearningPlatform.CourseCreation.Service.Util
{
    public class FindPlatformMemberOperation : FindEntitiesOperation
    {
        private String name;
        private List<PlatformMember> members;
        private CourseCreationDatabaseEntities context;

        public FindPlatformMemberOperation(string name, List<PlatformMember> members)
        {
            this.name = name;
            this.members = members;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public void FindEntities()
        {
            var foundMembers = (from m in context.PlatformMembers where (m.firstname+m.lastname).Contains(name) select m).ToList();
            members.AddRange(foundMembers.Select(m => new PlatformMember(m.firstname, m.lastname)).ToList());
        }
    }
}
