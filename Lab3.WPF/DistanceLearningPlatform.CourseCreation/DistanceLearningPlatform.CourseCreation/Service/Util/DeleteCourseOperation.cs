﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistanceLearningPlatform.CourseCreation.Model.Course;

namespace DistanceLearningPlatform.CourseCreation.Service.Util
{
    public class DeleteCourseOperation : DeleteEntityOperation
    {
        private string name;
        private CourseCreationDatabaseEntities context;

        public DeleteCourseOperation(string name)
        {
            this.name = name;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool DeleteEntity()
        {
            bool result = false;
            var foundEntity = context.Courses.FirstOrDefault(c => c.name == name);
            if (foundEntity != null)
            {
                deleteCourseFromCoursesAndMembers(foundEntity);
                context.Courses.Remove(foundEntity);
                context.SaveChanges();
                result = true;
            }
            return result;
        }
        private void deleteCourseFromCoursesAndMembers(Courses courses)
        {
            var entitiesFound = (from g in context.CoursesAndMembers where g.course_id == courses.id select g).ToList();
            context.CoursesAndMembers.RemoveRange(entitiesFound);
            context.SaveChanges();
        }
    }
}
