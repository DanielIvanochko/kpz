﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistanceLearningPlatform.CourseCreation.Model.Course;

namespace DistanceLearningPlatform.CourseCreation.Service.Util
{
    public class UpdateGroupOperation : UpdateEntityOperation
    {
        private string oldName;
        private Group group;
        private CourseCreationDatabaseEntities context;

        public UpdateGroupOperation(string oldName, Group group)
        {
            this.oldName = oldName;
            this.group = group;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool UpdateEntity()
        {
            bool result = false;
            var foundGroup = context.Groups.FirstOrDefault(g => g.name == oldName);
            if (foundGroup != null)
            {
                foundGroup.name = group.Name;
                var spec = context.Specializations.FirstOrDefault(s => s.name == group.Specialization.Name);
                if (spec != null)
                {
                    foundGroup.specializationId = spec.id;
                    foundGroup.Specializations = spec;
                }
                context.SaveChanges();
                var resultGroup = context.Groups.FirstOrDefault(g => g.name == group.Name);
                updateGroupsAndMembers(resultGroup, group);
                result = true;
            }
            return result;
        }
        public void updateGroupsAndMembers(Groups groups, Group group)
        {
            var membersToDelete = (from m in context.GroupsAndMembers where m.groupId == groups.id select m).ToList();
            context.GroupsAndMembers.RemoveRange(membersToDelete);
            foreach(var member in group.Members)
            {
                string fullname = member.FirstName + member.LastName;
                var foundMember = context.PlatformMembers.FirstOrDefault(m => (m.firstname + m.lastname) == fullname);
                if (foundMember != null)
                {
                    context.GroupsAndMembers.Add(new GroupsAndMembers() { groupId = groups.id, memberId = foundMember.id });
                }
            }
            context.SaveChanges();
        }
    }
}
