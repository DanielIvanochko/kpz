﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistanceLearningPlatform.CourseCreation.Model.Course;

namespace DistanceLearningPlatform.CourseCreation.Service.Util
{
    public class DeleteCategoryOperation : DeleteEntityOperation
    {
        private string name;
        private CourseCreationDatabaseEntities context;

        public DeleteCategoryOperation(string name)
        {
            this.name = name;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool DeleteEntity()
        {
            bool result = false;
            var foundEntity = context.CourseCategories.FirstOrDefault(c=>c.name==name);
            if (foundEntity != null)
            {
                DeleteCoursesWithCategory(foundEntity.id);
                context.CourseCategories.Remove(foundEntity);
                context.SaveChanges();
                result = true;
            }
            return result;
        }
        public void DeleteCoursesWithCategory(int categoryId)
        {
            var courses = context.Courses.Where(c => c.categoryId == categoryId).ToList();
            context.Courses.RemoveRange(courses);
            context.SaveChanges();
        }
    }
}
