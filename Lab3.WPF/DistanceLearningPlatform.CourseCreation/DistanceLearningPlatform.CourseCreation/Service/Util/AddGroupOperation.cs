﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistanceLearningPlatform.CourseCreation.Model.Course;

namespace DistanceLearningPlatform.CourseCreation.DBService.Util
{
    public class AddGroupOperation : AddEntityOperation
    {
        private CourseCreationDatabaseEntities context;
        private Group group;

        public AddGroupOperation(Group group)
        {
            this.group = group;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool addEntity()
        {
            bool result = false;
            if (context != null && group != null)
            {
                bool doesGroupExist = context.Groups.FirstOrDefault(g => g.name == group.Name) != null;
                var spec = context.Specializations.FirstOrDefault(s => s.name == group.Specialization.Name);
                if (!doesGroupExist && spec!=null)
                {
                    Groups groups = new Groups() { name = group.Name , specializationId = spec.id, Specializations=spec};
                    context.Groups.Add(groups);
                    context.SaveChanges();
                    var newGroup = context.Groups.FirstOrDefault(g => g.name == group.Name);
                    if(newGroup != null)
                    {
                        foreach (var member in group.Members)
                        {
                            string fullname = member.FirstName + member.LastName;
                            var newMember = context.PlatformMembers.FirstOrDefault(m => (m.firstname + m.lastname) == fullname);
                            context.GroupsAndMembers.Add(new GroupsAndMembers()
                            {
                                groupId = newGroup.id,
                                memberId = newMember.id
                            });

                        }
                        context.SaveChanges();
                        result = true;
                    }
                }
            }
            return result;
        }
    }
}
