﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistanceLearningPlatform.CourseCreation.Model.Course;

namespace DistanceLearningPlatform.CourseCreation.Service.Util
{
    public class FindGroupsOperation : FindEntitiesOperation
    {
        private String name;
        private List<Group> groups;
        private CourseCreationDatabaseEntities context;

        public FindGroupsOperation(string name, List<Group> groups)
        {
            this.name = name;
            this.groups = groups;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public void FindEntities()
        {
            var foundGroups = (from g in context.Groups where g.name.Contains(name) select g).ToList();
            foreach(var group in foundGroups)
            {
                var members = getMembersForGroup(group);
                groups.Add(new Group(group.name, members, new Specialization(group.Specializations.name)));
            }
        }
        private List<PlatformMember> getMembersForGroup(Groups groups)
        {
            List<PlatformMember> members = new List<PlatformMember>();
            int id = groups.id;
            var groupsAndMembersWithId = (from gm in context.GroupsAndMembers where gm.groupId == id select gm);
            foreach(var groupAndMember in groupsAndMembersWithId)
            {
                var member = context.PlatformMembers.FirstOrDefault(m => m.id == groupAndMember.memberId);
                if (member != null)
                {
                    members.Add(new PlatformMember(member.firstname, member.lastname));
                }
            }
            return members;
        }
    }
}
