﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistanceLearningPlatform.CourseCreation.Model.Course;

namespace DistanceLearningPlatform.CourseCreation.Service.Util
{
    public class UpdateSpecializationOperation : UpdateEntityOperation
    {
        private string oldName;
        private Specialization specialization;
        private CourseCreationDatabaseEntities context;

        public UpdateSpecializationOperation(string oldName, Specialization specialization)
        {
            this.oldName = oldName;
            this.specialization = specialization;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool UpdateEntity()
        {
            bool result = false;
            var foundSpec = context.Specializations.FirstOrDefault(s => s.name == oldName);
            if (foundSpec != null)
            {
                foundSpec.name = specialization.Name;
                context.SaveChanges();
                result = true;
            }
            return result;
        }
    }
}
