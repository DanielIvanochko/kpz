﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistanceLearningPlatform.CourseCreation.Model.Course;

namespace DistanceLearningPlatform.CourseCreation.Service.Util
{
    public interface DeleteEntityOperation
    {
        bool DeleteEntity();
        CourseCreationDatabaseEntities Context { get; set; }
    }
}
