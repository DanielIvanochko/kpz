﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.Service.Util
{
    public interface FindEntitiesOperation
    {
        void FindEntities();
        CourseCreationDatabaseEntities Context { get; set; }
    }
}
