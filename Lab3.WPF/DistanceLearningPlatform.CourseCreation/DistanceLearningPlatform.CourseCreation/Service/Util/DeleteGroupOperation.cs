﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistanceLearningPlatform.CourseCreation.Model.Course;


namespace DistanceLearningPlatform.CourseCreation.Service.Util
{
    public class DeleteGroupOperation : DeleteEntityOperation
    {
        private string name;
        private CourseCreationDatabaseEntities context;

        public DeleteGroupOperation(string name)
        {
            this.name = name;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool DeleteEntity()
        {
            bool result = false;
            var foundEntity = context.Groups.FirstOrDefault(c => c.name == name);
            if (foundEntity != null)
            {
                deleteGroupFromGroupAndMembers(foundEntity);
                context.Groups.Remove(foundEntity);
                context.SaveChanges();
                result = true;
            }
            return result;
        }
        private void deleteGroupFromGroupAndMembers(Groups groups)
        {
            var entitiesFound = (from g in context.GroupsAndMembers where g.groupId == groups.id select g).ToList();
            context.GroupsAndMembers.RemoveRange(entitiesFound);
            context.SaveChanges();
        }
    }
}
