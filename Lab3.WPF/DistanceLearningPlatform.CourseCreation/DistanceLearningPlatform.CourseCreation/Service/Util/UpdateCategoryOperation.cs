﻿using DistanceLearningPlatform.CourseCreation.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DistanceLearningPlatform.CourseCreation.Model.Course;

namespace DistanceLearningPlatform.CourseCreation.Service.Util
{
    public class UpdateCategoryOperation : UpdateEntityOperation
    {
        private string oldName;
        private CourseCategory category;
        private CourseCreationDatabaseEntities context;

        public UpdateCategoryOperation(string oldName, CourseCategory category)
        {
            this.oldName = oldName;
            this.category = category;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool UpdateEntity()
        {
            bool result = false;
            var foundCategory = context.CourseCategories.FirstOrDefault(c => c.name == oldName);
            if (foundCategory != null)
            {
                foundCategory.name = category.Name;
                context.SaveChanges();
                result = true;
            }
            return result;
        }
    }
}
