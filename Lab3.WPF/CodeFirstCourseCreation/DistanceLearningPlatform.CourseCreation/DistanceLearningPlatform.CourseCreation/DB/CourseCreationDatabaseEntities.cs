﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.DB
{
    public class CourseCreationDatabaseEntities : DbContext
    {
        public CourseCreationDatabaseEntities()
            : base("CodeFirstCourseCreationDatabase")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<CourseCreationDatabaseEntities, Migrations.Configuration>());
        }

        public DbSet<CourseCategories> CourseCategories { get; set; }
        public DbSet<Courses> Courses { get; set; }
        public DbSet<CoursesAndMembers> CoursesAndMembers { get; set; }
        public DbSet<Groups> Groups { get; set; }
        public DbSet<GroupsAndMembers> GroupsAndMembers { get; set; }
        public DbSet<PlatformMembers> PlatformMembers { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<Specializations> Specializations { get; set; }
        public DbSet<Users> Users { get; set; }
    }
}
