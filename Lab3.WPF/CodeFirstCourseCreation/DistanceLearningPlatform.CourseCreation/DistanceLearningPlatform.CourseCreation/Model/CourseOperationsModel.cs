﻿using DistanceLearningPlatform.CourseCreation.DBService;
using DistanceLearningPlatform.CourseCreation.DBService.Util;
using DistanceLearningPlatform.CourseCreation.Model.Course;
using DistanceLearningPlatform.CourseCreation.Service.Util;
using DistanceLearningPlatform.CourseCreation.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;
using System.Windows.Navigation;
using System.Xml.Serialization;

namespace DistanceLearningPlatform.CourseCreation.Model
{
    public class CourseOperationsModel
    {
        private User user;
        private CourseCreationService service;
        public User User { get => user; set => user = value; }
        public List<CourseCategory> Categories { get
            {
                List<CourseCategory> categories = new List<CourseCategory>();
                service.FindEntity(new FindCategoriesOperation("", categories));
                return categories;
            } }
        public List<Specialization> Specializations { get
            {
                List<Specialization> specializations = new List<Specialization>();
                service.FindEntity(new FindSpecializationsOperation("", specializations));
                return specializations;
            }
        }

        public CourseOperationsModel() {
            user = new User("user", "password", new Role("ADMIN"));
            service = new CourseCreationService();
        }


        public bool AddCourse(Course.Course course)
        {
            return service.AddEntity(new AddCourseOperation(course));
        }
        public bool AddGroup(Group group)
        {
            return service.AddEntity(new AddGroupOperation(group));
        }
        public bool AddMember(PlatformMember member)
        {
            return service.AddEntity(new AddMemberOperation(member));
        }
        public bool AddCategory(CourseCategory category)
        {
            return service.AddEntity(new AddCategoryOperation(category));
        }
        public bool AddSpecialization(Specialization specialization)
        {
            return service.AddEntity(new AddSpecializationOperation(specialization));
        }
        
        public List<Course.Course> FindCoursesWhereNameContains(String value)
        {
            List<Course.Course> courses = new List<Course.Course>();
            service.FindEntity(new FindCoursesOperation(value, courses));
            return courses;
        }
        public List<CourseCategory> FindCategoriesWhereNameContains(String value)
        {
            List<CourseCategory> categories = new List<CourseCategory>();
            service.FindEntity(new FindCategoriesOperation(value, categories));
            return categories;
        }
        public List<Specialization> FindSpecializationsWhereNameContains(String value)
        {
            List<Specialization> specializations = new List<Specialization>();
            service.FindEntity(new FindSpecializationsOperation(value, specializations));
            return specializations;
        }
        public List<PlatformMember> FindPlatformMembersWhereNameContains(String value)
        {
            List<PlatformMember> members = new List<PlatformMember>();
            service.FindEntity(new FindPlatformMemberOperation(value, members));
            return members;
        }
        public List<Group> FindGroupsWhereNameContains(String value)
        {
            List<Group> groups = new List<Group>();
            service.FindEntity(new FindGroupsOperation(value, groups));
            return groups;
        }
        public bool DeleteCourse(Course.Course course)
        {
            return service.DeleteEntity(new DeleteCourseOperation(course.Name));
        }
        public bool DeleteCategory(CourseCategory category)
        {
            return service.DeleteEntity(new DeleteCategoryOperation(category.Name));
        }
        public bool DeleteSpecialization(Specialization specialization)
        {
            return service.DeleteEntity(new DeleteSpecializationOperation(specialization.Name));
        }
        public bool DeleteMember(PlatformMember member)
        {
            return service.DeleteEntity(new DeleteMemberOperation(member.FirstName + " " + member.LastName));
        }
        public bool DeleteGroup(Group group)
        {
            return service.DeleteEntity(new DeleteGroupOperation(group.Name));

        }
        public bool UpdateCourse(string oldCourseName, Course.Course course)
        {
            return service.UpdateEntity(new UpdateCourseOperation(oldCourseName, course));
        }
        public bool UpdateGroup(string oldGroupName, Group group)
        {
            return service.UpdateEntity(new UpdateGroupOperation(oldGroupName, group));
        }
        public bool UpdateMember(PlatformMember member, String oldName)
        {
            return service.UpdateEntity(new UpdatePlatformMemberOperation(oldName, member));

        }
        public bool UpdateCategory(CourseCategory category, String oldName)
        {
            return service.UpdateEntity(new UpdateCategoryOperation(oldName, category));

        }
        public bool UpdateSpecialization(Specialization specialization, String oldName)
        {
            return service.UpdateEntity(new UpdateSpecializationOperation(oldName, specialization));
        }
    }
}
