﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.Model.Course
{
    public class Course
    {
        private String name;
        private String description;
        private CourseCategory category;
        private Specialization specialization;
        private List<PlatformMember> members;

        public Course()
        {
        }

        public Course(string name, string description, CourseCategory category, Specialization specialization, List<PlatformMember> members)
        {
            this.name = name;
            this.description = description;
            this.category = category;
            this.specialization = specialization;
            this.members = members;
        }

        public string Name { get => name; set => name = value; }
        public string Description { get => description; set => description = value; }
        public CourseCategory Category { get => category; set => category = value; }
        public Specialization Specialization { get => specialization; set => specialization = value; }
        public List<PlatformMember> Members { get => members; set => members = value; }
    }
}
