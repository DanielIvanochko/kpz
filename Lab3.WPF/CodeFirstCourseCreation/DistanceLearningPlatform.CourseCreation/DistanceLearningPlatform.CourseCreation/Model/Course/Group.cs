﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.Model.Course
{
    public class Group
    {
        private String name;
        private List<PlatformMember> members;
        private Specialization specialization;

        public Group(string name, List<PlatformMember> members, Specialization specialization)
        {
            this.name = name;
            this.members = members;
            this.specialization = specialization;
        }

        public string Name { get => name; set => name = value; }
        public List<PlatformMember> Members { get => members; set => members = value; }
        public Specialization Specialization { get => specialization; set => specialization = value; }
    }
}
