﻿using AutoMapper;
using DistanceLearningPlatform.CourseCreation.Model;
using DistanceLearningPlatform.CourseCreation.Model.Course;
using DistanceLearningPlatform.CourseCreation.ViewModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Documents;

namespace DistanceLearningPlatform.CourseCreation.ModelsMapper
{
    public class CourseEntitiesMapper
    {
        private MapperConfiguration configuration;
        private IMapper mapper;
        public CourseEntitiesMapper()
        {
            configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Course, CourseViewModel>();
                cfg.CreateMap<CourseViewModel, Course>();

                cfg.CreateMap<Group, GroupViewModel>();
                cfg.CreateMap<GroupViewModel, Group>();

                cfg.CreateMap<PlatformMember, PlatformMemberViewModel>();
                cfg.CreateMap<PlatformMemberViewModel, PlatformMember>();


                cfg.CreateMap<Role, RoleViewModel>();
                cfg.CreateMap<RoleViewModel, Role>();

                cfg.CreateMap<Specialization, SpecializationViewModel>();
                cfg.CreateMap<SpecializationViewModel, Specialization>();

                cfg.CreateMap<UserViewModel, User>();
                cfg.CreateMap<User, UserViewModel>();

                cfg.CreateMap<CourseCategory, CourseCategoryViewModel>();
                cfg.CreateMap<CourseCategoryViewModel, CourseCategory>();

                cfg.CreateMap<CourseOperationsModel, CourseOperationsViewModel>();
                cfg.CreateMap<CourseOperationsViewModel, CourseOperationsModel>();

            });
            mapper = configuration.CreateMapper();
        }

        public IMapper Mapper { get => mapper; set => mapper = value; }
    }
}
