﻿using Lab4.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Model.Course;

namespace Lab4.Util
{
    public class UpdatePlatformMemberOperation : UpdateEntityOperation
    {
        private string oldName;
        private PlatformMember member;
        private CourseCreationDatabaseEntities context;

        public UpdatePlatformMemberOperation(string oldName, PlatformMember member)
        {
            this.oldName = oldName;
            this.member = member;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool UpdateEntity()
        {
            bool result = false;
            var foundMember = context.PlatformMembers.FirstOrDefault(m => (m.firstname+" "+m.lastname) == oldName);
            if (foundMember != null)
            {
                foundMember.firstname = member.FirstName;
                foundMember.lastname = member.LastName;
                context.SaveChanges();
                result = true;
            }
            return result;
        }
    }
}
