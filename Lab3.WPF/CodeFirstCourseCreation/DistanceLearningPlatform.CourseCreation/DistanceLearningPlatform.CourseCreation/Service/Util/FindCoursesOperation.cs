﻿using Lab4.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Model.Course;

namespace Lab4.Util
{
    public class FindCoursesOperation : FindEntitiesOperation
    {
        private String name;
        private List<Course> courses;
        private CourseCreationDatabaseEntities context;

        public FindCoursesOperation(string name, List<Course> courses)
        {
            this.name = name;
            this.courses = courses;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public void FindEntities()
        {
            var foundCourses = (from c in context.Courses where c.name.Contains(name) select c).ToList();
            foreach (var course in foundCourses)
            {
                var members = getMembersForCourse(course);
                courses.Add(new Course(course.name, course.description,
                    new CourseCategory(course.CourseCategories.name),new Specialization(course.Specializations.name), members));
            }
        }
        private List<PlatformMember> getMembersForCourse(Courses courses)
        {
            List<PlatformMember> members = new List<PlatformMember>();
            int id = courses.id;
            var coursesAndMembers = (from cm in context.CoursesAndMembers where cm.CoursesId == id select cm).ToList();
            foreach (var courseAndMember in coursesAndMembers)
            {
                var member = context.PlatformMembers.FirstOrDefault(m => m.id == courseAndMember.PlatformMembersId);
                if (member != null)
                {
                    members.Add(new PlatformMember(member.firstname, member.lastname));
                }
            }
            return members;
        }
    }
}
