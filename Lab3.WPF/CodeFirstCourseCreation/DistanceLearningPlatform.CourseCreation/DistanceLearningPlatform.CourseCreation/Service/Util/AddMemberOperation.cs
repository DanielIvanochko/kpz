﻿using Lab4.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Model.Course;

namespace Lab4.Util
{
    public class AddMemberOperation : AddEntityOperation
    {
        private CourseCreationDatabaseEntities context;
        private PlatformMember member;

        public AddMemberOperation(PlatformMember member)
        {
            this.member = member;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context=value; }
        public PlatformMember ModelEntity { get => member; set => member=value; }

        public bool addEntity()
        {
            bool result = false;
            if(context != null && member != null)
            {
                Console.WriteLine("Checking whether member exists");
                string fullname = member.LastName + member.FirstName;
                bool doesMemberAlreadyExist = context.PlatformMembers
                    .FirstOrDefault(m => (m.lastname + m.firstname) == fullname) != null;
                Console.WriteLine("Got member..");
                if (!doesMemberAlreadyExist)
                {
                    Console.WriteLine("Saving to DB...");
                    context.PlatformMembers
                        .Add(new PlatformMembers() { firstname = member.FirstName, lastname = member.LastName });
                    context.SaveChanges();
                    result = true;
                    Console.WriteLine("Saved!");
                }
            }
            return result;
        }
    }
}
