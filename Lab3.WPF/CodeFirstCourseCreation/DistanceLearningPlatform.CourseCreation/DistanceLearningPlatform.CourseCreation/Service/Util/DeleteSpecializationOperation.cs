﻿using Lab4.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Util
{
    public class DeleteSpecializationOperation : DeleteEntityOperation
    {
        private string name;
        private CourseCreationDatabaseEntities context;

        public DeleteSpecializationOperation(string name)
        {
            this.name = name;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool DeleteEntity()
        {
            bool result = false;
            var foundEntity = context.Specializations.FirstOrDefault(c => c.name == name);
            if (foundEntity != null)
            {
                DeleteCoursesWithSpecialization(foundEntity.id);
                DeleteGroupsWithSpecialization(foundEntity.id);
                context.Specializations.Remove(foundEntity);
                context.SaveChanges();
                result = true;
            }
            return result;
        }
        public void DeleteCoursesWithSpecialization(int specId)
        {
            var courses = context.Courses.Where(c => c.SpecializationsId == specId).ToList();
            context.Courses.RemoveRange(courses);
            context.SaveChanges();
        }
        public void DeleteGroupsWithSpecialization(int specId)
        {
            var groups = context.Groups.Where(g => g.SpecializationsId == specId).ToList();
            context.Groups.RemoveRange(groups);
            context.SaveChanges();
        }
    }
}
