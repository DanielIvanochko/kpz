﻿using Lab4.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Util
{
    public interface AddEntityOperation
    {
        bool addEntity();
        CourseCreationDatabaseEntities Context { get; set; }
    }
}
