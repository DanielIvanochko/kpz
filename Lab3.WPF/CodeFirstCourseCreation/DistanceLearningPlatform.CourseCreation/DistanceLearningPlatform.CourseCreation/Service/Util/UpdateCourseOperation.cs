﻿using Lab4.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Model.Course;

namespace Lab4.Util
{
    public class UpdateCourseOperation : UpdateEntityOperation
    {
        private string oldName;
        private Course course;
        private CourseCreationDatabaseEntities context;

        public UpdateCourseOperation(string oldName, Course course)
        {
            this.oldName = oldName;
            this.course = course;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool UpdateEntity()
        {
            bool result = false;
            var foundCourse = context.Courses.FirstOrDefault(c => c.name == oldName);
            if (foundCourse != null)
            {
                foundCourse.name = course.Name;
                foundCourse.description = course.Description;
                var spec = context.Specializations.FirstOrDefault(s => s.name == course.Specialization.Name);
                if (spec != null)
                {
                    foundCourse.SpecializationsId = spec.id;
                }
                var category = context.CourseCategories.FirstOrDefault(c => c.name == course.Category.Name);
                if (category != null)
                {
                    foundCourse.CourseCategoriesId = category.id;
                }
                context.SaveChanges();
                var resultCourse = context.Courses.FirstOrDefault(c => c.name == course.Name);
                updateCourseAndMembers(resultCourse, course);
                result = true;
            }
            return result;
        }
        public void updateCourseAndMembers(Courses courses, Course course)
        {
            var membersToDelete = (from m in context.CoursesAndMembers where m.CoursesId == courses.id select m).ToList();
            context.CoursesAndMembers.RemoveRange(membersToDelete);
            foreach (var member in course.Members)
            {
                string fullname = member.FirstName + member.LastName;
                var foundMember = context.PlatformMembers.FirstOrDefault(m => (m.firstname + m.lastname) == fullname);
                if (foundMember != null)
                {
                    context.CoursesAndMembers.Add(new CoursesAndMembers() { CoursesId = courses.id
                        , PlatformMembersId= foundMember.id });
                }
            }
            context.SaveChanges();
        }
    }
}

