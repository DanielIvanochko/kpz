﻿using Lab4.DB;
using Lab4.Model.Course;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Util
{
    public class AddCourseOperation : AddEntityOperation
    {
        private CourseCreationDatabaseEntities context;
        private Course course;

        public AddCourseOperation(Course course)
        {
            this.course = course;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool addEntity()
        {
            bool result = false;
            if (context != null && course != null)
            {
                bool doesCourseAlreadyExist = context.Courses.FirstOrDefault(c => c.name == course.Name) != null;
                var category = context.CourseCategories.FirstOrDefault(c => c.name == course.Category.Name);
                var spec = context.Specializations.FirstOrDefault(s => s.name == course.Specialization.Name);
               
                if (!doesCourseAlreadyExist)
                {
                    Courses courses = new Courses() { name = course.Name, CourseCategoriesId=category.id, description=course.Description,
                        SpecializationsId = spec.id};
                    context.Courses.Add(courses);
                    context.SaveChanges();
                    var resultCourse = context.Courses.FirstOrDefault(c => c.name == course.Name);
                    if (resultCourse != null)
                    {
                        foreach (var member in course.Members)
                        {
                            string fullname = member.FirstName + member.LastName;
                            var newMember = context.PlatformMembers.FirstOrDefault(m => (m.firstname + m.lastname) == fullname);
                            if (newMember != null)
                            {
                                context.CoursesAndMembers.Add(new CoursesAndMembers()
                                {
                                    CoursesId = resultCourse.id,
                                    PlatformMembersId = newMember.id
                                });
                            }
                        }
                        context.SaveChanges();
                        result = true;
                    }
                }
            }
            return result;
        }
    }
}
