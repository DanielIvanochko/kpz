﻿using Lab4.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Util
{
    public interface UpdateEntityOperation
    {
        bool UpdateEntity();
        CourseCreationDatabaseEntities Context { get; set; }
    }
}
