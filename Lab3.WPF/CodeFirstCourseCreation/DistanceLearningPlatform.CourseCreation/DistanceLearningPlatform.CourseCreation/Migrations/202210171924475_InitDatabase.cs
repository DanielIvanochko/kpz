﻿namespace DistanceLearningPlatform.CourseCreation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CourseCategories",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        CourseCategoriesId = c.Int(),
                        name = c.String(),
                        SpecializationsId = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CourseCategories", t => t.CourseCategoriesId)
                .ForeignKey("dbo.Specializations", t => t.SpecializationsId)
                .Index(t => t.CourseCategoriesId)
                .Index(t => t.SpecializationsId);
            
            CreateTable(
                "dbo.CoursesAndMembers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        CoursesId = c.Int(),
                        PlatformMembersId = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Courses", t => t.CoursesId)
                .ForeignKey("dbo.PlatformMembers", t => t.PlatformMembersId)
                .Index(t => t.CoursesId)
                .Index(t => t.PlatformMembersId);
            
            CreateTable(
                "dbo.PlatformMembers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        firstname = c.String(),
                        lastname = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.GroupsAndMembers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        GroupsId = c.Int(),
                        PlatformMembersId = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Groups", t => t.GroupsId)
                .ForeignKey("dbo.PlatformMembers", t => t.PlatformMembersId)
                .Index(t => t.GroupsId)
                .Index(t => t.PlatformMembersId);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        SpecializationsId = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Specializations", t => t.SpecializationsId)
                .Index(t => t.SpecializationsId);
            
            CreateTable(
                "dbo.Specializations",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        login = c.String(),
                        password = c.String(),
                        RolesId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Roles", t => t.RolesId, cascadeDelete: true)
                .Index(t => t.RolesId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "RolesId", "dbo.Roles");
            DropForeignKey("dbo.GroupsAndMembers", "PlatformMembersId", "dbo.PlatformMembers");
            DropForeignKey("dbo.Groups", "SpecializationsId", "dbo.Specializations");
            DropForeignKey("dbo.Courses", "SpecializationsId", "dbo.Specializations");
            DropForeignKey("dbo.GroupsAndMembers", "GroupsId", "dbo.Groups");
            DropForeignKey("dbo.CoursesAndMembers", "PlatformMembersId", "dbo.PlatformMembers");
            DropForeignKey("dbo.CoursesAndMembers", "CoursesId", "dbo.Courses");
            DropForeignKey("dbo.Courses", "CourseCategoriesId", "dbo.CourseCategories");
            DropIndex("dbo.Users", new[] { "RolesId" });
            DropIndex("dbo.Groups", new[] { "SpecializationsId" });
            DropIndex("dbo.GroupsAndMembers", new[] { "PlatformMembersId" });
            DropIndex("dbo.GroupsAndMembers", new[] { "GroupsId" });
            DropIndex("dbo.CoursesAndMembers", new[] { "PlatformMembersId" });
            DropIndex("dbo.CoursesAndMembers", new[] { "CoursesId" });
            DropIndex("dbo.Courses", new[] { "SpecializationsId" });
            DropIndex("dbo.Courses", new[] { "CourseCategoriesId" });
            DropTable("dbo.Users");
            DropTable("dbo.Roles");
            DropTable("dbo.Specializations");
            DropTable("dbo.Groups");
            DropTable("dbo.GroupsAndMembers");
            DropTable("dbo.PlatformMembers");
            DropTable("dbo.CoursesAndMembers");
            DropTable("dbo.Courses");
            DropTable("dbo.CourseCategories");
        }
    }
}
