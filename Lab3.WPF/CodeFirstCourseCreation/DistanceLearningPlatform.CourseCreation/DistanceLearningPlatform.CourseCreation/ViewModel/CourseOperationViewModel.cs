﻿using DistanceLearningPlatform.CourseCreation.Model.Course;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class CourseOperationViewModel : ViewModelBase
    {
        private ObservableCollection<CourseCategoryViewModel> categories;
        private ObservableCollection<SpecializationViewModel> specializations;
        private ObservableCollection<PlatformMemberViewModel> platformMembers;
        private String windowTitle;
        private int selectedCategoryIndex;
        private int selectedSpecializationIndex;
        private string courseName;
        private string description;
        private string oldCourseName;
        private ICommand courseOperationCommand;
        private ICommand addMemberCommand;
        private ICommand deleteMemberCommand;
        private string memberFirstName;
        private string memberLastName;
        private PlatformMemberViewModel selectedMember;

        public CourseOperationViewModel(ObservableCollection<CourseCategoryViewModel> categories, ObservableCollection<SpecializationViewModel> specializations, ObservableCollection<PlatformMemberViewModel> platformMembers)
        {
            this.categories = categories;
            this.specializations = specializations;
            this.platformMembers = platformMembers;
            windowTitle = "Create Course";
            selectedCategoryIndex = 0;
            selectedSpecializationIndex = 0;
            courseOperationCommand = new Command.Command(CreateCourseCommand);
            addMemberCommand = new Command.Command(AddMember);
            deleteMemberCommand = new Command.Command(DeleteMember);
        }
        public CourseOperationViewModel(CourseViewModel courseViewModel, CourseOperationsViewModel courseOperationsViewModel)
        {
            windowTitle = "Update Course";
            CourseName = courseViewModel.Name;
            oldCourseName = courseViewModel.Name;
            Description = courseViewModel.Description;
            Categories = courseOperationsViewModel.Categories;
            Specializations = courseOperationsViewModel.Specializations;
            PlatformMembers = courseViewModel.Members;

            setCategoryIndex(courseViewModel);
            setSpecializationIndex(courseViewModel);

            courseOperationCommand = new Command.Command(UpdateCourse);
            addMemberCommand = new Command.Command(AddMember);
            deleteMemberCommand = new Command.Command(DeleteMember);
        }
        private void setCategoryIndex(CourseViewModel courseViewModel)
        {
            var category = courseViewModel.Category;
            var selectedCategory = Categories.FirstOrDefault(c => c.Name == category.Name);
            if (selectedCategory != null)
            {
                SelectedCategoryIndex = Categories.IndexOf(selectedCategory);
            }
        }
        private void setSpecializationIndex(CourseViewModel courseViewModel)
        {
            var specialization = courseViewModel.Specialization;
            var selectedSpecialization = Specializations.FirstOrDefault(s => s.Name == specialization.Name);
            if (selectedSpecialization != null)
            {
                SelectedSpecializationIndex = Specializations.IndexOf(selectedSpecialization);
            }
        }
        public void UpdateCourse(object args)
        {
            CourseCategoryViewModel selectedCategory = categories.ElementAt(SelectedCategoryIndex);
            SpecializationViewModel specializationViewModel = specializations.ElementAt(SelectedSpecializationIndex);
            CourseViewModel courseViewModel = new CourseViewModel(CourseName, Description, selectedCategory, specializationViewModel, PlatformMembers);
            bool result = App.CourseOperationsViewModel.UpdateCourse(courseViewModel, OldCourseName);
            String message = result ? "Course updated" : "Course WAS NOT updated";
            MessageBox.Show(message);
        }
        public ObservableCollection<CourseCategoryViewModel> Categories { get => categories; set { categories = value; OnPropertyChanged("Categories"); } }
        public ObservableCollection<SpecializationViewModel> Specializations { get => specializations; set { specializations = value; OnPropertyChanged("Specializations"); } }
        public ObservableCollection<PlatformMemberViewModel> PlatformMembers { get => platformMembers; set { platformMembers = value; OnPropertyChanged("PlatformMembers"); } }

        public string WindowTitle { get => windowTitle; set { windowTitle = value; OnPropertyChanged("WindowTitle"); } }

        public int SelectedCategoryIndex { get => selectedCategoryIndex; set { selectedCategoryIndex = value; OnPropertyChanged("SelectedCategoryIndex"); } }

        public int SelectedSpecializationIndex { get => selectedSpecializationIndex; set { selectedSpecializationIndex = value; OnPropertyChanged("SelectedSpecializationIndex"); } }

        public string CourseName { get => courseName; set { courseName = value; OnPropertyChanged("CourseName"); } }
        public string Description { get => description; set { description = value; OnPropertyChanged("Description"); } }

        public string OldCourseName { get => oldCourseName; set => oldCourseName = value; }

        public ICommand CourseOperationCommand { get => courseOperationCommand; set { courseOperationCommand = value; OnPropertyChanged("CourseOperationCommand"); } }

        public ICommand AddMemberCommand { get => addMemberCommand; set { addMemberCommand = value; OnPropertyChanged("AddMemberCommand"); } }

        public string MemberFirstName { get => memberFirstName; set { memberFirstName = value; OnPropertyChanged("MemberFirstName"); } }
        public string MemberLastName { get => memberLastName; set { memberLastName = value; OnPropertyChanged("MemberLastName"); } }

        public PlatformMemberViewModel SelectedMember { get => selectedMember; set { selectedMember = value; OnPropertyChanged("SelectedMember"); } }

        public ICommand DeleteMemberCommand { get => deleteMemberCommand; set { deleteMemberCommand = value; OnPropertyChanged("DeleteMemberCommand"); } }

        public void CreateCourseCommand(object args)
        {
            CourseCategoryViewModel selectedCategory = categories.ElementAt(SelectedCategoryIndex);
            SpecializationViewModel specializationViewModel = specializations.ElementAt(SelectedSpecializationIndex);
            CourseViewModel courseViewModel = new CourseViewModel(CourseName, Description, selectedCategory, specializationViewModel, PlatformMembers);
            bool result = App.CourseOperationsViewModel.AddCourse(courseViewModel);
            String message = result ? "Course created" : "Course WAS NOT created";
            MessageBox.Show(message);
        }
        public void AddMember(object args)
        {
            PlatformMemberViewModel platformMemberViewModel = new PlatformMemberViewModel(MemberFirstName, MemberLastName);
            PlatformMembers.Add(platformMemberViewModel);
            MemberFirstName = "";
            MemberLastName = "";
        }
        public void DeleteMember(object args)
        {
            PlatformMembers.Remove(SelectedMember);
        }
    }
}
