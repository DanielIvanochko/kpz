﻿using DistanceLearningPlatform.CourseCreation.Model.Course;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class GroupViewModel:ViewModelBase
    {
        private String name;
        private SpecializationViewModel specialization;
        private ObservableCollection<PlatformMemberViewModel> members;
        public GroupViewModel() { }
        public GroupViewModel(string name, ObservableCollection<PlatformMemberViewModel> members, SpecializationViewModel specialization)
        {
            this.name = name;
            this.members = members;
            this.specialization = specialization;
        }

        public string Name { get => name; set { name = value; OnPropertyChanged("Name"); } }
        public ObservableCollection<PlatformMemberViewModel> Members { get => members; set
            {
                members = value;
                OnPropertyChanged("Members");
            }
        }

        public SpecializationViewModel Specialization { get => specialization; set { specialization = value; OnPropertyChanged("Specialization"); } }
    }
}
