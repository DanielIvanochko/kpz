﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class SpecializationsFoundViewModel : CourseEntitiesFoundViewModel<SpecializationViewModel>
    {
        private ObservableCollection<SpecializationViewModel> specializationsFound;
        private CourseOperationsViewModel courseOperationsViewModel;

        public SpecializationsFoundViewModel()
        {
        }

        public SpecializationsFoundViewModel(CourseOperationsViewModel courseOperationsViewModel)
        {
            this.courseOperationsViewModel = courseOperationsViewModel;
            this.specializationsFound = new ObservableCollection<SpecializationViewModel>();
        }

        public override ObservableCollection<SpecializationViewModel> EntitiesFound 
        { get => specializationsFound; set { specializationsFound = value; OnPropertyChanged("EntitiesFound"); } }


        public override void FindCourseEntitiesWhereNameContains(string value)
        {
            EntitiesFound = courseOperationsViewModel.FindSpecializationsWhereNameContains(value);
        }
        public void DeleteSpecialization(object args)
        {
            var courseViewModel = (SpecializationViewModel)SelectedItem;
            bool result = App.CourseOperationsViewModel.DeleteSpecialization(courseViewModel);
            String message = result ? "Specialization deleted" : "Specialization WAS NOT deleted";
            MessageBox.Show(message);
        }
        public void UpdateSpecialization(object args)
        {
            var specializationViewModel = (SpecializationViewModel)SelectedItem;
            SpecializationOperationViewModel viewModel = new SpecializationOperationViewModel(App.CourseOperationsViewModel, specializationViewModel);
            SpecializationOperationWindow window = new SpecializationOperationWindow(viewModel);
            window.Show();
        }
    }
}
