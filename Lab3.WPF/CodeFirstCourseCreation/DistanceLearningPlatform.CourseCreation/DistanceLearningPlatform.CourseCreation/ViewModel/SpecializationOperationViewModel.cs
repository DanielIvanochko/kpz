﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class SpecializationOperationViewModel : ViewModelBase
    {
        private string specializationName;
        private string windowTitle;
        private ICommand specializationOperationCommand;
        private CourseOperationsViewModel courseOperationsViewModel;
        private string oldName;
        public SpecializationOperationViewModel(CourseOperationsViewModel courseOperationsViewModel)
        {
            this.courseOperationsViewModel = courseOperationsViewModel;
            windowTitle = "Create Specialization";
            specializationOperationCommand = new Command.Command(AddSpecialization);
        }
        public SpecializationOperationViewModel(CourseOperationsViewModel courseOperationsViewModel, SpecializationViewModel specializationViewModel)
        {
            windowTitle = "Update Specialization";
            this.courseOperationsViewModel = courseOperationsViewModel;
            oldName = specializationViewModel.Name;
            specializationName = specializationViewModel.Name;
            specializationOperationCommand = new Command.Command(UpdateSpecialization);
        }
        public string SpecializationName { get => specializationName; set { specializationName = value; OnPropertyChanged("SpecializationName"); } }
        public string WindowTitle { get => windowTitle; set { windowTitle = value; OnPropertyChanged("WindowTitle"); } }
        public ICommand SpecializationOperationCommand { get => specializationOperationCommand; set { specializationOperationCommand = value; OnPropertyChanged("CategoryOperationCommand"); } }
        public void AddSpecialization(object args)
        {
            bool result = courseOperationsViewModel.AddSpecialization(new SpecializationViewModel(specializationName));
            String message = result ? "Specialization created" : "Specialization WAS NOT created";
            MessageBox.Show(message);
        }
        public void UpdateSpecialization(object args)
        {
            SpecializationViewModel specializationViewModel = new SpecializationViewModel(specializationName);
            bool result = courseOperationsViewModel.UpdateSpecialization(specializationViewModel, oldName);
            string message = result ? "Specialization updated" : "Specialization WAS NOT updated";
            MessageBox.Show(message);
        }
    }
}
