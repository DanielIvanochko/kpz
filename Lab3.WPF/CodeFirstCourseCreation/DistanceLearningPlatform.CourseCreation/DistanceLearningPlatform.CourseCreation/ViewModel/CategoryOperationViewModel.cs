﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class CategoryOperationViewModel : ViewModelBase
    {
        private string categoryName;
        private string windowTitle;
        private ICommand categoryOperationCommand;
        private CourseOperationsViewModel courseOperationsViewModel;
        private string oldName;
        public CategoryOperationViewModel(CourseOperationsViewModel courseOperationsViewModel)
        {
            this.courseOperationsViewModel = courseOperationsViewModel;
            windowTitle = "Create Category";
            categoryOperationCommand = new Command.Command(AddCategory);
        }
        public CategoryOperationViewModel(CourseOperationsViewModel courseOperationsViewModel, CourseCategoryViewModel categoryViewModel)
        {
            windowTitle = "Update Category";
            this.courseOperationsViewModel = courseOperationsViewModel;
            oldName = categoryViewModel.Name;
            categoryName = categoryViewModel.Name;
            categoryOperationCommand = new Command.Command(UpdateCategory);
        }
        public string CategoryName { get => categoryName; set { categoryName = value; OnPropertyChanged("CategoryName"); } }
        public string WindowTitle { get => windowTitle; set { windowTitle = value; OnPropertyChanged("WindowTitle"); } }
        public ICommand CategoryOperationCommand { get => categoryOperationCommand; set { categoryOperationCommand = value; OnPropertyChanged("CategoryOperationCommand"); } }
        public void AddCategory(object args)
        {
            bool result = courseOperationsViewModel.AddCategory(new CourseCategoryViewModel(categoryName));
            String message = result ? "Category created" : "Category WAS NOT created";
            MessageBox.Show(message);
        }
        public void UpdateCategory(object args)
        {
            CourseCategoryViewModel categoryViewModel = new CourseCategoryViewModel(categoryName);
            bool result = courseOperationsViewModel.UpdateCategory(categoryViewModel, oldName);
            string message = result ? "Category updated" : "Category WAS NOT updated";
            MessageBox.Show(message);
        }
    }
}
