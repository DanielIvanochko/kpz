﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class CourseCategoryViewModel : ViewModelBase
    {
        private String name;

        public CourseCategoryViewModel(string name)
        {
            this.name = name;
        }

        public string Name { get => name; 
            set{
                name = value;
                OnPropertyChanged("Name");
            }
        }
    }
}
