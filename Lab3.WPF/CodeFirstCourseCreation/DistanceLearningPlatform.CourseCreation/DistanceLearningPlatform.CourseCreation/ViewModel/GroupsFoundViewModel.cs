﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class GroupsFoundViewModel : CourseEntitiesFoundViewModel<GroupViewModel>
    {
        private ObservableCollection<GroupViewModel> groupsFound;
        private CourseOperationsViewModel courseOperationsViewModel;
        public GroupsFoundViewModel() { }

        public GroupsFoundViewModel(CourseOperationsViewModel courseOperationsViewModel)
        {
            this.courseOperationsViewModel = courseOperationsViewModel;
            this.groupsFound = new ObservableCollection<GroupViewModel>();
        }

        public override ObservableCollection<GroupViewModel> EntitiesFound { get => groupsFound; set { groupsFound = value; OnPropertyChanged("EntitiesFound"); } }


        public override void FindCourseEntitiesWhereNameContains(string value)
        {
            EntitiesFound = courseOperationsViewModel.FindGroupsWhereNameContains(value);
        }
        public void DeleteGroup(object args)
        {
            GroupViewModel groupViewModel = (GroupViewModel)SelectedItem;
            bool result = App.CourseOperationsViewModel.DeleteGroup(groupViewModel);
            String message = result ? "Group deleted" : "Group WAS NOT deleted";
            MessageBox.Show(message);
        }
        public void UpdateGroup(object args)
        {
            var groupViewModel = (GroupViewModel)SelectedItem;
            var groupOperationViewModel = new GroupOperationViewModel(groupViewModel, App.CourseOperationsViewModel);
            GroupOperationWindow groupOperationWindow = new GroupOperationWindow(groupOperationViewModel);
            groupOperationWindow.Show();
        }
    }
}
