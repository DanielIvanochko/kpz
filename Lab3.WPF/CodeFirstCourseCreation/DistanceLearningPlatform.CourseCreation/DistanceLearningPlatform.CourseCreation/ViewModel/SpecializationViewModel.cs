﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class SpecializationViewModel : ViewModelBase
    {
        private String name;

        public SpecializationViewModel(string name)
        {
            this.name = name;
        }

        public string Name { get => name; set { name = value; OnPropertyChanged("Name"); } }
    }
}
