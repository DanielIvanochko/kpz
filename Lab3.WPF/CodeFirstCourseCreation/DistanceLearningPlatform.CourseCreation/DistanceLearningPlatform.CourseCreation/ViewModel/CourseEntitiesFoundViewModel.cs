﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public abstract class CourseEntitiesFoundViewModel<T> : ViewModelBase
    {
        private static object selectedItem;
        public static object SelectedItem { get => selectedItem;
            set { selectedItem = value; } }
        public abstract void FindCourseEntitiesWhereNameContains(String value);
        public abstract ObservableCollection<T> EntitiesFound { set; get; }
    }
}
