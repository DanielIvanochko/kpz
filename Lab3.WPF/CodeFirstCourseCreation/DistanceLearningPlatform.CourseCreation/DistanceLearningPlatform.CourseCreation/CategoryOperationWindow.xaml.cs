﻿using DistanceLearningPlatform.CourseCreation.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DistanceLearningPlatform.CourseCreation
{
    /// <summary>
    /// Interaction logic for CategoryOperationWindow.xaml
    /// </summary>
    public partial class CategoryOperationWindow : Window
    {
        public CategoryOperationWindow()
        {
            InitializeComponent();
            this.DataContext = new CategoryOperationViewModel(App.CourseOperationsViewModel);
        }
        public CategoryOperationWindow(CategoryOperationViewModel categoryOperationViewModel)
        {
            InitializeComponent();
            this.DataContext = categoryOperationViewModel;
        }
        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
