﻿using AutoMapper;
using Lab4.Model;
using Lab4.Model.Course;
using Lab4.ModelsMapper;
using Lab4.ViewModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Lab4
{
    public class CourseEntitiesMapper : ICourseEntitiesMapper
    {
        private MapperConfiguration configuration;
        private IMapper mapper;
        public CourseEntitiesMapper()
        {
            configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Course, CourseViewModel>();
                cfg.CreateMap<CourseViewModel, Course>();

                cfg.CreateMap<Group, GroupViewModel>();
                cfg.CreateMap<GroupViewModel, Group>();

                cfg.CreateMap<PlatformMember, PlatformMemberViewModel>();
                cfg.CreateMap<PlatformMemberViewModel, PlatformMember>();


                cfg.CreateMap<Specialization, SpecializationViewModel>();
                cfg.CreateMap<SpecializationViewModel, Specialization>();


                cfg.CreateMap<CourseCategory, CourseCategoryViewModel>();
                cfg.CreateMap<CourseCategoryViewModel, CourseCategory>();

            });
            mapper = configuration.CreateMapper();
        }

        public IMapper Mapper { get => mapper; set => mapper = value; }
    }
}
