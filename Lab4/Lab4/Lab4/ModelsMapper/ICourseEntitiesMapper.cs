﻿using AutoMapper;

namespace Lab4.ModelsMapper
{
    public interface ICourseEntitiesMapper
    {
        IMapper Mapper { get; set; }
    }
}
