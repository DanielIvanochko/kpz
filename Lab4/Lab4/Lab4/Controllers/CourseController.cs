﻿using AutoMapper;
using Lab4.DBService.Util;
using Lab4.Model.Course;
using Lab4.ModelsMapper;
using Lab4.Service;
using Lab4.Service.Util;
using Lab4.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace Lab4.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CourseController : Controller
    {
        private readonly ICourseCreationService service;
        private readonly ICourseEntitiesMapper courseEntitiesMapper;
        private readonly IMapper mapper;

        public CourseController(ICourseCreationService service, ICourseEntitiesMapper courseEntitiesMapper)
        {
            this.service = service;
            this.courseEntitiesMapper = courseEntitiesMapper;
            this.mapper = courseEntitiesMapper.Mapper;
        }

        [HttpGet("{name}")]
        public List<Course> FindCourses(String name)
        {
            List<Course> courses = new List<Course>();
            service.FindEntity(new FindCoursesOperation(name, courses));
            return courses;
        }
        [HttpPost]
        public IActionResult CreateCourse(CourseViewModel courseViewModel)
        {
            var course = mapper.Map<CourseViewModel, Course>(courseViewModel);
            return service.AddEntity(new AddCourseOperation(course)) ? Ok() : BadRequest();
        }
        [HttpPut("{oldName}")]
        public IActionResult UpdateCourse(String oldName, CourseViewModel courseViewModel)
        {
            var course = mapper.Map<CourseViewModel, Course>(courseViewModel);
            return service.UpdateEntity(new UpdateCourseOperation(oldName, course)) ? Ok() : NotFound();
        }
        [HttpDelete("{name}")]
        public IActionResult DeleteCourse(String name)
        {
            return service.DeleteEntity(new DeleteCourseOperation(name)) ? Ok() : NotFound();
        }
    }
}
