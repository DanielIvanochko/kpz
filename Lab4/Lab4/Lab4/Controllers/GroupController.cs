﻿using Lab4.DBService.Util;
using Lab4.Model.Course;
using Lab4.Service.Util;
using Lab4.Service;
using Microsoft.AspNetCore.Mvc;
using Lab4.ModelsMapper;
using AutoMapper;
using Lab4.ViewModel;

namespace Lab4.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GroupController : Controller
    {
        private readonly ICourseCreationService service;
        private readonly ICourseEntitiesMapper courseEntitiesMapper;
        private readonly IMapper mapper;

        public GroupController(ICourseCreationService service, ICourseEntitiesMapper courseEntitiesMapper)
        {
            this.service = service;
            this.courseEntitiesMapper = courseEntitiesMapper;
            this.mapper = courseEntitiesMapper.Mapper;
        }

        [HttpGet("{name}")]
        public List<Group> FindGroups(String name)
        {
            List<Group> groups = new List<Group>();
            service.FindEntity(new FindGroupsOperation(name, groups));
            return groups;
        }
        [HttpPost]
        public IActionResult CreateGroup(GroupViewModel groupViewModel)
        {
            var group = mapper.Map<GroupViewModel, Group>(groupViewModel);
            return service.AddEntity(new AddGroupOperation(group)) ? Ok() : BadRequest();
        }
        [HttpPut("{oldName}")]
        public IActionResult UpdateGroup(String oldName, GroupViewModel groupViewModel)
        {
            var group = mapper.Map<GroupViewModel, Group>(groupViewModel);
            return service.UpdateEntity(new UpdateGroupOperation(oldName, group)) ? Ok() : NotFound();
        }
        [HttpDelete("{name}")]
        public IActionResult DeleteGroup(String name)
        {
            return service.DeleteEntity(new DeleteGroupOperation(name)) ? Ok() : BadRequest();
        }
    }
}
