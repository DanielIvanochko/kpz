﻿using AutoMapper;
using Lab4.DBService.Util;
using Lab4.Model.Course;
using Lab4.ModelsMapper;
using Lab4.Service;
using Lab4.Service.Util;
using Lab4.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Lab4.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MemberController : Controller
    {
        private readonly ICourseCreationService service;
        private readonly ICourseEntitiesMapper courseEntitiesMapper;
        private readonly IMapper mapper;

        public MemberController(ICourseCreationService service, ICourseEntitiesMapper courseEntitiesMapper)
        {
            this.service = service;
            this.courseEntitiesMapper = courseEntitiesMapper;
            this.mapper = courseEntitiesMapper.Mapper;
        }

        [HttpGet("{name}")]
        public List<PlatformMember> FindMemberByName(String name)
        {
            List<PlatformMember> platformMembers = new List<PlatformMember>();
            service.FindEntity(new FindPlatformMemberOperation(name, platformMembers));
            return platformMembers;
        }
        [HttpPost]
        public IActionResult CreateMember(PlatformMemberViewModel platformMemberViewModel)
        {
            IActionResult result = BadRequest();
            var platformMember = mapper.Map<PlatformMemberViewModel, PlatformMember>(platformMemberViewModel);
            if(service.AddEntity(new AddMemberOperation(platformMember)))
            {
                result = Ok();
            }
            return result;
        }
        [HttpPut("{oldName}")]
        public IActionResult UpdateMember(String oldName, PlatformMemberViewModel platformMemberViewModel)
        {
            var platformMember = mapper.Map<PlatformMemberViewModel, PlatformMember>(platformMemberViewModel);
            return service.UpdateEntity(new UpdatePlatformMemberOperation(oldName, platformMember)) ? Ok() : NotFound();
        }
        [HttpDelete("{name}")]
        public IActionResult DeleteMember(String name)
        {
            return service.DeleteEntity(new DeleteMemberOperation(name)) ? Ok() : NotFound();
        }
    }
}
