﻿using AutoMapper;
using Lab4.DBService.Util;
using Lab4.Model.Course;
using Lab4.ModelsMapper;
using Lab4.Service;
using Lab4.Service.Util;
using Lab4.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace Lab4.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : Controller
    {
        private readonly ICourseCreationService service;
        private readonly ICourseEntitiesMapper courseEntitiesMapper;
        private readonly IMapper mapper;

        public CategoryController(ICourseCreationService service, ICourseEntitiesMapper courseEntitiesMapper)
        {
            this.service = service;
            this.courseEntitiesMapper = courseEntitiesMapper;
            this.mapper = courseEntitiesMapper.Mapper;
        }

        [HttpGet("{name}")]
        public List<CourseCategory> FindCategories(String name)
        {
            List<CourseCategory> courseCategories = new List<CourseCategory>();
            service.FindEntity(new FindCategoriesOperation(name, courseCategories));
            return courseCategories;
        }
        [HttpPost]
        public IActionResult CreateCategory(CourseCategoryViewModel courseCategoryViewModel)
        {
            var courseCategory = mapper.Map<CourseCategoryViewModel, CourseCategory>(courseCategoryViewModel);
            return service.AddEntity(new AddCategoryOperation(courseCategory)) ? Ok() : BadRequest();
        }
        [HttpPut("{oldName}")]
        public IActionResult UpdateCategory(String oldName, CourseCategoryViewModel categoryViewModel)
        {
            var courseCategory = mapper.Map<CourseCategoryViewModel, CourseCategory>(categoryViewModel);
            return service.UpdateEntity(new UpdateCategoryOperation(oldName, courseCategory)) ? Ok() : NotFound();
        }
        [HttpDelete("{name}")]
        public IActionResult DeleteCategory(String name)
        {
            return service.DeleteEntity(new DeleteCategoryOperation(name)) ? Ok() : BadRequest();
        }
    }
}
