﻿using Lab4.DBService.Util;
using Lab4.Model.Course;
using Lab4.Service.Util;
using Lab4.Service;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Lab4.ModelsMapper;
using Lab4.ViewModel;

namespace Lab4.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SpecializationController : Controller
    {
        private readonly ICourseCreationService service;
        private readonly ICourseEntitiesMapper courseEntitiesMapper;
        private readonly IMapper mapper;

        public SpecializationController(ICourseCreationService service, ICourseEntitiesMapper courseEntitiesMapper)
        {
            this.service = service;
            this.courseEntitiesMapper = courseEntitiesMapper;
            this.mapper = courseEntitiesMapper.Mapper;
        }
        [HttpGet("{name}")]
        public List<Specialization> FindCategories(String name)
        {
            List<Specialization> specializations = new List<Specialization>();
            service.FindEntity(new FindSpecializationsOperation(name, specializations));
            return specializations;
        }
        [HttpPost]
        public IActionResult CreateSpecialization(SpecializationViewModel specializationViewModel)
        {
            var specialization = mapper.Map<SpecializationViewModel, Specialization>(specializationViewModel);
            return service.AddEntity(new AddSpecializationOperation(specialization)) ? Ok() : BadRequest();
        }
        [HttpPut("{oldName}")]
        public IActionResult UpdateSpecialization(String oldName, SpecializationViewModel specializationViewModel)
        {
            var specialization = mapper.Map<SpecializationViewModel, Specialization>(specializationViewModel);
            return service.UpdateEntity(new UpdateSpecializationOperation(oldName, specialization)) ? Ok() : NotFound();
        }
        [HttpDelete("{name}")]
        public IActionResult DeleteSpecialization(String name)
        {
            return service.DeleteEntity(new DeleteSpecializationOperation(name)) ? Ok() : BadRequest();
        }
    }
}
