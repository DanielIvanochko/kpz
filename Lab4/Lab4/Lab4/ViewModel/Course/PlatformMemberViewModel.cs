﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.ViewModel
{
    public class PlatformMemberViewModel 
    {
        private String firstName;
        private String lastName;

        public PlatformMemberViewModel(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public string FirstName { get => firstName; set { firstName = value;  } }
        public string LastName { get => lastName; set { lastName = value;  } }

        public string Name { get => (FirstName + " " + LastName); }
    }
}
