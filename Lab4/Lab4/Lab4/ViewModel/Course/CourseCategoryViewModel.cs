﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.ViewModel
{
    public class CourseCategoryViewModel
    {
        private String name;

        public CourseCategoryViewModel(string name)
        {
            this.name = name;
        }

        public string Name { get => name; 
            set{
                name = value;
            }
        }
    }
}
