﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.ViewModel
{
    public class GroupViewModel
    {
        private String name;
        private SpecializationViewModel specialization;
        private ObservableCollection<PlatformMemberViewModel> members;
        public GroupViewModel() { }
        public GroupViewModel(string name, ObservableCollection<PlatformMemberViewModel> members, SpecializationViewModel specialization)
        {
            this.name = name;
            this.members = members;
            this.specialization = specialization;
        }

        public string Name { get => name; set { name = value;  } }
        public ObservableCollection<PlatformMemberViewModel> Members { get => members; set
            {
                members = value;
                
            }
        }

        public SpecializationViewModel Specialization { get => specialization; set { specialization = value;  } }
    }
}
