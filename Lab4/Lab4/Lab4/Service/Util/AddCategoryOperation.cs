﻿using Lab4.DB;
using Lab4.Model.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.DBService.Util
{
    public class AddCategoryOperation:AddEntityOperation
    {

        private CourseCreationDatabaseEntities context;
        private CourseCategory category;

        public AddCategoryOperation(CourseCategory category)
        {
            this.category = category;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool addEntity()
        {
            bool result = false;
            if (context != null && category != null)
            {
                bool doesCategoryExist = context.CourseCategories.FirstOrDefault(c => c.name == category.Name) != null;
                if (!doesCategoryExist)
                {
                    CourseCategories courseCategories = new CourseCategories() { name = category.Name };
                    context.CourseCategories.Add(courseCategories);
                    context.SaveChanges();
                    result = true;
                }
            }
            return result;
        }
    }
}
