﻿using Lab4.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Model.Course;

namespace Lab4.Service.Util
{
    public interface DeleteEntityOperation
    {
        bool DeleteEntity();
        CourseCreationDatabaseEntities Context { get; set; }
    }
}
