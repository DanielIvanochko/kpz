﻿using Lab4.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Service.Util
{
    public class DeleteMemberOperation : DeleteEntityOperation
    {
        private string name;
        private CourseCreationDatabaseEntities context;

        public DeleteMemberOperation(string name)
        {
            this.name = name;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool DeleteEntity()
        {
            bool result = false;
            var foundEntity = context.PlatformMembers.FirstOrDefault(p => (p.firstname+" "+p.lastname) == name);
            if (foundEntity != null)
            {
                deleteMemberFromCoursesAndMembers(foundEntity);
                deleteMemberFromGroupsAndMembers(foundEntity);
                context.SaveChanges();
                context.PlatformMembers.Remove(foundEntity);
                context.SaveChanges();
                result = true;
            }
            return result;
        }
        public void deleteMemberFromGroupsAndMembers(PlatformMembers member)
        {
            var entities = (from e in context.GroupsAndMembers where e.PlatformMembersId == member.id select e).ToList();
            context.GroupsAndMembers.RemoveRange(entities);
        }
        public void deleteMemberFromCoursesAndMembers(PlatformMembers member)
        {
            var entities = (from e in context.CoursesAndMembers where e.PlatformMembersId == member.id select e).ToList();
            context.CoursesAndMembers.RemoveRange(entities);
        }
    }
}
