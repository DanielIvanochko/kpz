﻿using Lab4.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Model.Course;

namespace Lab4.Service.Util
{
    public class FindCategoriesOperation : FindEntitiesOperation
    {
        private String name;
        private List<CourseCategory> categories;
        private CourseCreationDatabaseEntities context;

        public FindCategoriesOperation(string name, List<CourseCategory> categories)
        {
            this.name = name;
            this.categories = categories;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context=value; }

        public void FindEntities()
        {
            var foundCategories = (from c in context.CourseCategories where c.name.Contains(name) select c).ToList();
            categories.AddRange(foundCategories.Select(c => new CourseCategory(c.name)).ToList());
        }
    }
}
