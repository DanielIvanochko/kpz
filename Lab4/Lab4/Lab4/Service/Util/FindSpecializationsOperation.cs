﻿using Lab4.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Model.Course;


namespace Lab4.Service.Util
{
    public class FindSpecializationsOperation : FindEntitiesOperation
    {
        private String name;
        private List<Specialization> specializations;
        private CourseCreationDatabaseEntities context;

        public FindSpecializationsOperation(string name, List<Specialization> specializations)
        {
            this.name = name;
            this.specializations = specializations;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public void FindEntities()
        {
            var foundSpecs = (from s in context.Specializations where s.name.Contains(name) select s).ToList();
            specializations.AddRange(foundSpecs.Select(s => new Specialization(s.name)).ToList());
        }
    }
}
