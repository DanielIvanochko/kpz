﻿using Lab4.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Model.Course;


namespace Lab4.Service.Util
{
    public class DeleteGroupOperation : DeleteEntityOperation
    {
        private string name;
        private CourseCreationDatabaseEntities context;

        public DeleteGroupOperation(string name)
        {
            this.name = name;
        }

        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }

        public bool DeleteEntity()
        {
            bool result = false;
            var foundEntity = context.Groups.FirstOrDefault(c => c.name == name);
            if (foundEntity != null)
            {
                deleteGroupFromGroupAndMembers(foundEntity);
                context.Groups.Remove(foundEntity);
                context.SaveChanges();
                result = true;
            }
            return result;
        }
        private void deleteGroupFromGroupAndMembers(Groups groups)
        {
            var entitiesFound = (from g in context.GroupsAndMembers where g.GroupsId == groups.id select g).ToList();
            context.GroupsAndMembers.RemoveRange(entitiesFound);
            context.SaveChanges();
        }
    }
}
