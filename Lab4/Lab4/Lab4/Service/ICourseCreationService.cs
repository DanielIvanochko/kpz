﻿using Lab4.DB;
using Lab4.DBService.Util;
using Lab4.Service.Util;

namespace Lab4.Service
{
    public interface ICourseCreationService
    {
        bool AddEntity(AddEntityOperation addEntityOperation);
        void FindEntity(FindEntitiesOperation findEntitiesOperation);
        bool UpdateEntity(UpdateEntityOperation updateEntityOperation);
        bool DeleteEntity(DeleteEntityOperation deleteEntityOperation);
        CourseCreationDatabaseEntities Context { get; set; }
    }
}
