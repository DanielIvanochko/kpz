﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Lab4.DB;
using Lab4.DBService.Util;
using Lab4.Service;
using Lab4.Service.Util;

namespace Lab4.DBService
{
    public class CourseCreationService : ICourseCreationService
    {
        private CourseCreationDatabaseEntities context;

        public CourseCreationService()
        {
            this.context = new CourseCreationDatabaseEntities();
        }

        /*
         * Add operations according to task:
         *  course adding,
         *  categories adding,
         *  specializations adding,
         *  groups adding,
         *  members adding to system,
         *  members adding to group,
         *  members adding to course
         * 
         * update operations:
         *  default entities (member, course, category, group, specialization),
         * delete operations:
         *  default entities (member, course, category, group, specialization)
         *  corner case:
         *      if the entity is member, we should delete member from all courses and groups
         */
        public bool AddEntity(AddEntityOperation addEntityOperation)
        {
            addEntityOperation.Context = Context;
            return addEntityOperation.addEntity();
        }
        public void FindEntity(FindEntitiesOperation findEntitiesOperation)
        {
            findEntitiesOperation.Context = Context;
            findEntitiesOperation.FindEntities();
        }
        public bool UpdateEntity(UpdateEntityOperation updateEntityOperation)
        {
            updateEntityOperation.Context = Context;
            return updateEntityOperation.UpdateEntity();
        }
        public bool DeleteEntity(DeleteEntityOperation deleteEntityOperation)
        {
            deleteEntityOperation.Context = Context;
            return deleteEntityOperation.DeleteEntity();
        }
        public CourseCreationDatabaseEntities Context { get => context; set => context = value; }
    }
}
