using Lab4;
using Lab4.DBService;
using Lab4.ModelsMapper;
using Lab4.Service;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var services = builder.Services;
services.AddCors(c =>
{
    c.AddPolicy("AllowOrigin", policy => policy.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
});

services.AddScoped<ICourseCreationService, CourseCreationService>();
services.AddScoped<ICourseEntitiesMapper, CourseEntitiesMapper>();

var app = builder.Build();

app.UseCors(policyBuilder => policyBuilder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
