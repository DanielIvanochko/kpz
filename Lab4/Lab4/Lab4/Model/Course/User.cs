﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Model.Course
{
    public class User
    {
        private String login;
        private String password;
        private Role role;

        public User(string login, string password, Role role)
        {
            this.login = login;
            this.password = password;
            this.role = role;
        }

        public string Login { get => login; set => login = value; }
        public string Password { get => password; set => password = value; }
        public Role Role { get => role; set => role = value; }
    }
}
