import { Hello } from "./test";

export const HELLOS: Hello[] = [
    {id : 1, name:"first name"},
    {id : 2, name:"second name"}
]