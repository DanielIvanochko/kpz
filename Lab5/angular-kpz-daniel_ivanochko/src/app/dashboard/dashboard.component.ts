import { Component, OnInit } from '@angular/core';
import { HelloService } from '../hello.service';
import { Hello } from '../test';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  hellos : Hello[] = [];
  constructor(private helloService : HelloService) { }

  ngOnInit(): void {
    this.getHellos();
  }
  getHellos(): void{
    this.helloService.getHellos()
    .subscribe(hellos=>this.hellos = hellos.slice(1,5));
  }
}
