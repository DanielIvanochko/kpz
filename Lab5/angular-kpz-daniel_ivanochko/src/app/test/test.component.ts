import { Component, OnInit } from '@angular/core';
import { Hello } from '../test';
import { HELLOS } from '../mock-test';
import { HelloService } from '../hello.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  hellos : Hello[] = [];
  constructor(private helloService: HelloService,
    private messageService : MessageService) { }
  selectedHello? : Hello;
  ngOnInit(): void {
    this.getHeroes();
  }
  getHeroes() : void{
    this.helloService.getHellos()
    .subscribe(hellos => this.hellos = hellos);
  }
}
