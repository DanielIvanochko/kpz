import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestComponent } from './test/test.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HelloDetailComponent } from './hello-detail/hello-detail.component';

const routes: Routes = [
  {path:'', redirectTo:"/dashboard", pathMatch:'full'},
  {path:'dashboard', component: DashboardComponent},
  {path:'hellos', component: TestComponent},
  {path:'detail/:id', component: HelloDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
