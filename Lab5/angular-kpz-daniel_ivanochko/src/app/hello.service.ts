import { Injectable } from '@angular/core';
import { Hello } from './test';
import { HELLOS } from './mock-test';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class HelloService {

  constructor(private messageService: MessageService) { }
  getHellos(): Observable<Hello[]>{
    const hellos = of(HELLOS);
    this.messageService.add('Fetched hellos');
    return hellos;
  }
  getHello(id:number): Observable<Hello>{
    const hello = HELLOS.find(h=>h.id === id)!;
    this.messageService.add(`HelloService : fetched hello id=${id}`)
    return of(hello);
  }
}
