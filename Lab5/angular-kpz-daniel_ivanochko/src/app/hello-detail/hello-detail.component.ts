import { Component, OnInit } from '@angular/core';
import { Hello } from '../test';
import { Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HelloService } from '../hello.service';
@Component({
  selector: 'app-hello-detail',
  templateUrl: './hello-detail.component.html',
  styleUrls: ['./hello-detail.component.scss']
})
export class HelloDetailComponent implements OnInit {

  @Input() hello?:Hello;
  constructor(
    private route: ActivatedRoute,
    private helloService : HelloService,
    private location : Location
  ) { }

  ngOnInit(): void {
    this.getHero();
  }
  getHero() : void{
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.helloService.getHello(id).subscribe(hello=>this.hello  = hello);
  }
  goBack(): void{
    this.location.back();
  }
}
