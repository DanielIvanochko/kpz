import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { MemberCreatorComponent } from './component/member-creator/member-creator.component';
import { FormsModule } from '@angular/forms';
import { MembersFinderComponent } from './component/members-finder/members-finder.component';
import { MemberUpdaterComponent } from './component/member-updater/member-updater.component';
import { SpecializationCreatorComponent } from './component/specialization-creator/specialization-creator.component';
import { SpecializationsFinderComponent } from './component/specializations-finder/specializations-finder.component';
import { SpecializationUpdaterComponent } from './component/specialization-updater/specialization-updater.component';
import { CategoriesFinderComponent } from './component/category-finder/category-finder.component';
import { CategoryUpdaterComponent } from './component/category-updater/category-updater.component';
import { CategoryCreatorComponent } from './component/category-creator/category-creator.component';
import { GroupCreatorComponent } from './component/group-creator/group-creator.component';
import { GroupsFinderComponent } from './component/groups-finder/groups-finder.component';
import { GroupUpdaterComponent } from './component/group-updater/group-updater.component';
import { CourseCreatorComponent } from './component/course-creator/course-creator.component';
import { CourseUpdaterComponent } from './component/course-updater/course-updater.component';
import { CourseFinderComponent } from './component/course-finder/course-finder.component';

@NgModule({
  declarations: [
    AppComponent,
    MemberCreatorComponent,
    MembersFinderComponent,
    MemberUpdaterComponent,
    SpecializationCreatorComponent,
    SpecializationsFinderComponent,
    SpecializationUpdaterComponent,
    CategoriesFinderComponent,
    CategoryUpdaterComponent,
    CategoryCreatorComponent,
    GroupCreatorComponent,
    GroupsFinderComponent,
    GroupUpdaterComponent,
    CourseCreatorComponent,
    CourseUpdaterComponent,
    CourseFinderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
