import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MemberCreatorComponent } from './component/member-creator/member-creator.component';
import { MembersFinderComponent } from './component/members-finder/members-finder.component';
import { MemberUpdaterComponent } from './component/member-updater/member-updater.component';
import { SpecializationCreatorComponent } from './component/specialization-creator/specialization-creator.component';
import { SpecializationsFinderComponent } from './component/specializations-finder/specializations-finder.component';
import { SpecializationUpdaterComponent } from './component/specialization-updater/specialization-updater.component';
import { CategoryCreatorComponent } from './component/category-creator/category-creator.component';
import { CategoriesFinderComponent } from './component/category-finder/category-finder.component';
import { CategoryUpdaterComponent } from './component/category-updater/category-updater.component';
import { GroupCreatorComponent } from './component/group-creator/group-creator.component';
import { GroupsFinderComponent } from './component/groups-finder/groups-finder.component';
import { GroupUpdaterComponent } from './component/group-updater/group-updater.component';
import { CourseCreatorComponent } from './component/course-creator/course-creator.component';
import { CourseFinderComponent } from './component/course-finder/course-finder.component';
import { CourseUpdaterComponent } from './component/course-updater/course-updater.component';

const routes: Routes = [
  {path:'member-creator', component:MemberCreatorComponent},
  {path:'members-finder', component:MembersFinderComponent},
  {path:'member-updater/:firstName/:lastName', component:MemberUpdaterComponent},
  {path:'spec-creator', component:SpecializationCreatorComponent},
  {path:'specs-finder', component: SpecializationsFinderComponent},
  {path:'spec-updater/:name', component: SpecializationUpdaterComponent},
  {path:'category-creator', component:CategoryCreatorComponent},
  {path:'categories-finder', component: CategoriesFinderComponent},
  {path:'category-updater/:name', component: CategoryUpdaterComponent},
  {path:'group-creator', component: GroupCreatorComponent},
  {path:'groups-finder', component: GroupsFinderComponent},
  {path:'group-updater/:name', component: GroupUpdaterComponent}, 
  {path:'course-creator', component:CourseCreatorComponent},
  {path:'course-finder', component: CourseFinderComponent},
  {path:'course-updater/:name', component: CourseUpdaterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
