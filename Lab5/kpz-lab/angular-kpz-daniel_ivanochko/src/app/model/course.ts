import { CourseCategory } from "./course-category";
import { PlatformMember } from "./platform-member";
import { Specialization } from "./specialization";

export interface Course{
    name: string;
    description: string;
    category: CourseCategory;
    specialization: Specialization;
    members: PlatformMember[];
}