export interface PlatformMember {
    firstName: string;
    lastName: string;
}
