import { PlatformMember } from "./platform-member";
import { Specialization } from "./specialization";

export interface Group{
    name: string;
    members: PlatformMember[];
    specialization : Specialization;
}