import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupsFinderComponent } from './groups-finder.component';

describe('GroupsFinderComponent', () => {
  let component: GroupsFinderComponent;
  let fixture: ComponentFixture<GroupsFinderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupsFinderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GroupsFinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
