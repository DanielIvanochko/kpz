import { Component, OnInit } from '@angular/core';
import { Group } from 'src/app/model/group';
import { GroupService } from 'src/app/service/group.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-groups-finder',
  templateUrl: './groups-finder.component.html',
  styleUrls: ['./groups-finder.component.scss']
})
export class GroupsFinderComponent implements OnInit {

  groups: Group[] = [];
  name: string = '';
  constructor(private groupService: GroupService, private route: Router) { }

  ngOnInit(): void {
  }
  getGroups(): void{
    this.groupService.getGroups(this.name).subscribe(groups=>this.groups = groups);
  }
  deleteGroup(group: Group): void{
    this.groupService.deleteGroup(group.name).subscribe(()=>{
      alert('Group '+group.name+ " was deleted!");
    }, ()=>{
      alert('Group '+group.name+ " was NOT deleted!");
    });
  }
  updateGroup(group: Group): void{
    this.route.navigateByUrl('/group-updater/'+group.name);
  }
}
