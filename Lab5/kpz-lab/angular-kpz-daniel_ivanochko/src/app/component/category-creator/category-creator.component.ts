import { Component, OnInit } from '@angular/core';
import { CourseCategory } from 'src/app/model/course-category';
import { CategoryService } from 'src/app/service/category.service';

@Component({
  selector: 'app-category-creator',
  templateUrl: './category-creator.component.html',
  styleUrls: ['./category-creator.component.scss']
})
export class CategoryCreatorComponent implements OnInit {
  category: CourseCategory = {
    name : ''
  }
  constructor(private categoryService: CategoryService) { }
  
  ngOnInit(): void {
  }
  createCategory(): void{
    this.categoryService.createCategory(this.category).subscribe(()=>{
      alert('Course category created!');
    },()=>{
      alert('Course category was not created!');
    });
  }
}

