import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecializationsFinderComponent } from './specializations-finder.component';

describe('SpecializationsFinderComponent', () => {
  let component: SpecializationsFinderComponent;
  let fixture: ComponentFixture<SpecializationsFinderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecializationsFinderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SpecializationsFinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
