import { Component, OnInit } from '@angular/core';
import { Specialization } from 'src/app/model/specialization';
import { SpecializationService } from 'src/app/service/specialization-service.service';

@Component({
  selector: 'app-specializations-finder',
  templateUrl: './specializations-finder.component.html',
  styleUrls: ['./specializations-finder.component.scss']
})
export class SpecializationsFinderComponent implements OnInit {
  specs: Specialization[] = [];
  name: string = '';
  constructor(private specService: SpecializationService) { }

  ngOnInit(): void {
  }
  getSpecs(): void{
    this.specService.getSpecializations(this.name).subscribe(specs=>this.specs = specs);
  }
  deleteSpec(specName: string): void{
    this.specService.deleteSpecialization(specName).subscribe();
  }
}
