import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseUpdaterComponent } from './course-updater.component';

describe('CourseUpdaterComponent', () => {
  let component: CourseUpdaterComponent;
  let fixture: ComponentFixture<CourseUpdaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseUpdaterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CourseUpdaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
