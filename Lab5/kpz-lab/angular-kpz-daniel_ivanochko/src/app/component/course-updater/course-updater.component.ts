import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Course } from 'src/app/model/course';
import { CourseService } from 'src/app/service/course.service';
import { Location } from '@angular/common';
import { PlatformMember } from 'src/app/model/platform-member';

@Component({
  selector: 'app-course-updater',
  templateUrl: './course-updater.component.html',
  styleUrls: ['./course-updater.component.scss']
})
export class CourseUpdaterComponent implements OnInit {

  course: Course;
  oldName: string;
  firstName: string = '';
  lastName: string = '';
  constructor(private courseService: CourseService,
              private route: ActivatedRoute,
              private location: Location) {
    this.oldName = this.route.snapshot.paramMap.get('name')!;
    this.course = {name:'', members:[], specialization: {name:''}, category: {name:''}, description:''};
    courseService.getCourses(this.oldName).subscribe(courses=>this.course = courses[0]);
  }

  ngOnInit(): void {
  }
  addMember(): void{
    this.course.members.push({firstName:this.firstName,
       lastName: this.lastName});
  }
  deleteMember(member: PlatformMember): void{
    const index = this.course.members.indexOf(member);
    this.course.members.splice(index, 1);
  }
  updateCourse(): void{
    this.courseService.updateCourse(this.oldName, this.course)
    .subscribe(()=>{
      alert('Course was updated!');
      this.getBack();
    }, ()=>{
      alert('Course was not updated!');
    });
  }
  getBack(): void{
    this.location.back();
  }
}

