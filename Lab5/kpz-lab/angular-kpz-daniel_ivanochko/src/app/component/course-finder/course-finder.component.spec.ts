import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseFinderComponent } from './course-finder.component';

describe('CourseFinderComponent', () => {
  let component: CourseFinderComponent;
  let fixture: ComponentFixture<CourseFinderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseFinderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CourseFinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
