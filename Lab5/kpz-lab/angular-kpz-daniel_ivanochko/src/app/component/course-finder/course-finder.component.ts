import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Course } from 'src/app/model/course';
import { CourseService } from 'src/app/service/course.service';

@Component({
  selector: 'app-course-finder',
  templateUrl: './course-finder.component.html',
  styleUrls: ['./course-finder.component.scss']
})
export class CourseFinderComponent implements OnInit {

  courses: Course[] = [];
  name: string = '';
  constructor(private courseService: CourseService, private route: Router) { }

  ngOnInit(): void {
  }
  getCourses(): void{
    this.courseService.getCourses(this.name).subscribe(courses=>this.courses = courses);
  }
  deleteCourse(course: Course): void{
    this.courseService.deleteCourse(course.name).subscribe(()=>{
      alert('Course '+course.name+ " was deleted!");
    }, ()=>{
      alert('Course '+course.name+ " was NOT deleted!");
    });
  }
  updateCourse(course: Course): void{
    this.route.navigateByUrl('/course-updater/'+course.name);
  }
}
