import { Component, OnInit } from '@angular/core';
import { Group } from 'src/app/model/group';
import { GroupService } from 'src/app/service/group.service';
import { PlatformMember } from 'src/app/model/platform-member';

@Component({
  selector: 'app-group-creator',
  templateUrl: './group-creator.component.html',
  styleUrls: ['./group-creator.component.scss']
})
export class GroupCreatorComponent implements OnInit {

  group: Group = {
    name:'',
    members:[],
    specialization: {name: ''}
  }
  firstName: string = '';
  lastName: string = '';
  constructor(private groupService: GroupService) { }

  ngOnInit(): void {
  }
  createGroup(): void{
    if(this.group.name === '' || this.group.specialization.name === ''){
      alert('Please specify all the fields')
    }else{
      this.groupService.createGroup(this.group).subscribe(()=>{
        alert('Group created!');
      }, ()=>{
        alert('Group was not created!');
      });
    }
  }
  addMember(): void{
    this.group.members.push({firstName: this.firstName, lastName: this.lastName});
  }
  deleteMember(member: PlatformMember): void{
    const index = this.group.members.indexOf(member);
    if(index !== -1){
      this.group.members.splice(index, 1);
    }
  }
}
