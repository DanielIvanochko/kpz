import { Component, OnInit } from '@angular/core';
import { Course } from 'src/app/model/course';
import { PlatformMember } from 'src/app/model/platform-member';
import { CourseService } from 'src/app/service/course.service';

@Component({
  selector: 'app-course-creator',
  templateUrl: './course-creator.component.html',
  styleUrls: ['./course-creator.component.scss']
})
export class CourseCreatorComponent implements OnInit {

  course: Course = {
    name:'',
    members:[],
    specialization: {name: ''},
    category:{name:''},
    description: ''
  }
  firstName: string = '';
  lastName: string = '';
  constructor(private courseService: CourseService) { }

  ngOnInit(): void {
  }
  createCourse(): void{
    if(this.course.name === '' || this.course.specialization.name === '' || this.course.category.name===''){
      alert('Please specify all the fields')
    }else{
      this.courseService.createCourse(this.course).subscribe(()=>{
        alert('Course created!');
      }, ()=>{
        alert('Course was not created!');
      });
    }
  }
  addMember(): void{
    this.course.members.push({firstName: this.firstName, lastName: this.lastName});
  }
  deleteMember(member: PlatformMember): void{
    const index = this.course.members.indexOf(member);
    if(index !== -1){
      this.course.members.splice(index, 1);
    }
  }
}