import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpecializationService } from 'src/app/service/specialization-service.service';
import { Location } from '@angular/common';
import { Specialization } from 'src/app/model/specialization';

@Component({
  selector: 'app-specialization-updater',
  templateUrl: './specialization-updater.component.html',
  styleUrls: ['./specialization-updater.component.scss']
})
export class SpecializationUpdaterComponent implements OnInit {

  spec: Specialization = {
    name: ''
  }
  name: string = '';
  constructor(private route: ActivatedRoute, private specService: SpecializationService,
    private location: Location) {
      this.name = this.route.snapshot.paramMap.get('name')!;
    }

  ngOnInit(): void {
  }
  updateSpec(): void{
    this.specService.updateSpecialization(this.name, this.spec).subscribe(()=>{
      alert('Specialization updated!');
      this.getBack();
    }, ()=>{
      alert('Specialization was not updated :(');
    })
  }
  getBack(): void{
    this.location.back();
  }
}
