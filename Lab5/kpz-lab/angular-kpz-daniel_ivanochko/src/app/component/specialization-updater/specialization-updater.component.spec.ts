import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecializationUpdaterComponent } from './specialization-updater.component';

describe('SpecializationUpdaterComponent', () => {
  let component: SpecializationUpdaterComponent;
  let fixture: ComponentFixture<SpecializationUpdaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecializationUpdaterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SpecializationUpdaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
