import { Component, OnInit } from '@angular/core';
import { Group } from 'src/app/model/group';
import { GroupService } from 'src/app/service/group.service';
import { ActivatedRoute} from '@angular/router';
import { PlatformMember } from 'src/app/model/platform-member';
import { Location } from '@angular/common';
@Component({
  selector: 'app-group-updater',
  templateUrl: './group-updater.component.html',
  styleUrls: ['./group-updater.component.scss']
})
export class GroupUpdaterComponent implements OnInit {

  group: Group;
  oldName: string;
  firstName: string = '';
  lastName: string = '';
  //state: Observable<Object>;
  constructor(private groupService: GroupService,
              private route: ActivatedRoute,
              private location: Location) {
    this.oldName = this.route.snapshot.paramMap.get('name')!;
    this.group = {name:'', members:[], specialization: {name:''}};
    groupService.getGroups(this.oldName).subscribe(groups=>this.group = groups[0]);
  }

  ngOnInit(): void {
  }
  addMember(): void{
    this.group.members.push({firstName:this.firstName,
       lastName: this.lastName});
  }
  deleteMember(member: PlatformMember): void{
    const index = this.group.members.indexOf(member);
    this.group.members.splice(index, 1);
  }
  updateGroup(): void{
    this.groupService.updateGroup(this.oldName, this.group)
    .subscribe(()=>{
      alert('Group was updated!');
      this.getBack();
    }, ()=>{
      alert('Group was not updated!');
    });
  }
  getBack(): void{
    this.location.back();
  }
}
