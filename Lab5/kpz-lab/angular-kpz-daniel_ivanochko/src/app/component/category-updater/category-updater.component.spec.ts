import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryUpdaterComponent } from './category-updater.component';

describe('CategoryUpdaterComponent', () => {
  let component: CategoryUpdaterComponent;
  let fixture: ComponentFixture<CategoryUpdaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryUpdaterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CategoryUpdaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
