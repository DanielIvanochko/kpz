import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CourseCategory } from 'src/app/model/course-category';
import { Location } from '@angular/common';
import { CategoryService } from 'src/app/service/category.service';

@Component({
  selector: 'app-category-updater',
  templateUrl: './category-updater.component.html',
  styleUrls: ['./category-updater.component.scss']
})
export class CategoryUpdaterComponent implements OnInit {

  category: CourseCategory = {
    name: ''
  }
  name: string = '';
  constructor(private route: ActivatedRoute, private categoryService: CategoryService,
    private location: Location) {
      this.name = this.route.snapshot.paramMap.get('name')!;
    }

  ngOnInit(): void {
  }
  updateSpec(): void{
    this.categoryService.updateCategory(this.name, this.category).subscribe(()=>{
      alert('Course category updated!');
      this.getBack();
    }, ()=>{
      alert('Course category was not updated :(');
    })
  }
  getBack(): void{
    this.location.back();
  }
}
