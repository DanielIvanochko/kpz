import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberUpdaterComponent } from './member-updater.component';

describe('MemberUpdaterComponent', () => {
  let component: MemberUpdaterComponent;
  let fixture: ComponentFixture<MemberUpdaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MemberUpdaterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MemberUpdaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
