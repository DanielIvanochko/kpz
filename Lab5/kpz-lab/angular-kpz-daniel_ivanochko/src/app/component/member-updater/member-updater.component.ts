import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlatformMemberService } from 'src/app/service/platform-member.service';
import {PlatformMember} from 'src/app/model/platform-member';
import { Location } from '@angular/common';
@Component({
  selector: 'app-member-updater',
  templateUrl: './member-updater.component.html',
  styleUrls: ['./member-updater.component.scss']
})
export class MemberUpdaterComponent implements OnInit {
  
  member : PlatformMember = {
    firstName:'',
    lastName: ''
  }
  firstName: string = '';
  lastName: string = '';
  constructor(private route: ActivatedRoute, private memberService: PlatformMemberService,
    private location: Location) {
    this.firstName = this.route.snapshot.paramMap.get('firstName')!;
    this.lastName = this.route.snapshot.paramMap.get('lastName')!;
  }

  ngOnInit(): void {
  }
  updateMember(): void{
    const name = this.firstName + " " + this.lastName;
    this.memberService.updateMember(name, this.member).subscribe(()=>{
      alert('member updated!')
      this.location.back();
    }, ()=>{
      alert('member was not updated!')
    });
  }
  getBack(): void{
    this.location.back();
  }
}
