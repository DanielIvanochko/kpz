import { Component, OnInit } from '@angular/core';
import { PlatformMember } from 'src/app/model/platform-member';
import { PlatformMemberService } from 'src/app/service/platform-member.service';

@Component({
  selector: 'app-members-finder',
  templateUrl: './members-finder.component.html',
  styleUrls: ['./members-finder.component.scss']
})
export class MembersFinderComponent implements OnInit {

  members: PlatformMember[] = [];
  name: string = "";
  constructor(private memberService: PlatformMemberService) { }

  ngOnInit(): void {
  }
  findMembers() : void{
    this.memberService.findMembers(this.name).subscribe(members=>this.members = members);
  }
  deleteMember(member: PlatformMember): void{
    console.log('delete member clicked');
    this.memberService.deleteMember(member.firstName+" "+member.lastName).subscribe();
  }

}
