import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MembersFinderComponent } from './members-finder.component';

describe('MembersFinderComponent', () => {
  let component: MembersFinderComponent;
  let fixture: ComponentFixture<MembersFinderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MembersFinderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MembersFinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
