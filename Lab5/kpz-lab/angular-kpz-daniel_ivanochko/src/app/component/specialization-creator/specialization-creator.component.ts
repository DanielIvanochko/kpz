import { Component, OnInit } from '@angular/core';
import { Specialization } from 'src/app/model/specialization';
import { SpecializationService } from 'src/app/service/specialization-service.service';

@Component({
  selector: 'app-specialization-creator',
  templateUrl: './specialization-creator.component.html',
  styleUrls: ['./specialization-creator.component.scss']
})
export class SpecializationCreatorComponent implements OnInit {
  spec: Specialization = {
    name : ''
  }
  constructor(private specService: SpecializationService) { }
  
  ngOnInit(): void {
  }
  createSpecialization(): void{
    this.specService.createSpecialization(this.spec).subscribe(()=>{
      alert('specialization created!');
    },()=>{
      alert('specialization was not created!');
    });
  }
}
