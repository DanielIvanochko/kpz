import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecializationCreatorComponent } from './specialization-creator.component';

describe('SpecializationCreatorComponent', () => {
  let component: SpecializationCreatorComponent;
  let fixture: ComponentFixture<SpecializationCreatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecializationCreatorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SpecializationCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
