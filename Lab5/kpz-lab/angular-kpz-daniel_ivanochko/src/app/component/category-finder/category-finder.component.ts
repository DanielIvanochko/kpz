import { Component, OnInit } from '@angular/core';
import { CourseCategory } from 'src/app/model/course-category';
import { CategoryService } from 'src/app/service/category.service';

@Component({
  selector: 'app-category-finder',
  templateUrl: './category-finder.component.html',
  styleUrls: ['./category-finder.component.scss']
})
export class CategoriesFinderComponent implements OnInit {
  categories: CourseCategory[] = [];
  name: string = '';
  constructor(private categoryService: CategoryService) { }

  ngOnInit(): void {
  }
  getCategories(): void{
    this.categoryService.getCategories(this.name).subscribe(categories=>this.categories = categories);
  }
  deleteCategory(categoryName: string): void{
    this.categoryService.deleteCategory(categoryName).subscribe();
  }
}
