import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryFinderComponent } from './category-finder.component';

describe('CategoryFinderComponent', () => {
  let component: CategoryFinderComponent;
  let fixture: ComponentFixture<CategoryFinderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryFinderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CategoryFinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
