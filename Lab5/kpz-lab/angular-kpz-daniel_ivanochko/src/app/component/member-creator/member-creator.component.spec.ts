import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberCreatorComponent } from './member-creator.component';

describe('MemberCreatorComponent', () => {
  let component: MemberCreatorComponent;
  let fixture: ComponentFixture<MemberCreatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MemberCreatorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MemberCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
