import { Component, OnInit } from '@angular/core';
import { PlatformMemberService } from 'src/app/service/platform-member.service';
import { PlatformMember } from 'src/app/model/platform-member';
@Component({
  selector: 'app-member-creator',
  templateUrl: './member-creator.component.html',
  styleUrls: ['./member-creator.component.scss']
})
export class MemberCreatorComponent implements OnInit {

  member : PlatformMember = {
    firstName:'',
    lastName: ''
  }
  constructor(private memberService : PlatformMemberService) { }

  ngOnInit(): void {
  }
  createMember():void{
    console.log(this.member);
    debugger;
    this.memberService.createMember(this.member).subscribe(()=>{}, ()=>{});
  }
}
