import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Course } from '../model/course';

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  private SERVER_ADDRESS: string  = "https://localhost:7129/Course";
  private header: HttpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private httpClient: HttpClient) { }
  createCourse(course: Course): Observable<any>{
    return this.httpClient.post<any>(this.SERVER_ADDRESS, course, {headers:this.header});
  }
  getCourses(name: string): Observable<Course[]>{
    return this.httpClient.get<Course[]>(this.SERVER_ADDRESS+"/"+name, {headers: this.header});
  }
  updateCourse(name: string, course: Course): Observable<any>{
    return this.httpClient.put<any>(this.SERVER_ADDRESS+"/"+name, course, {headers: this.header});
  }
  deleteCourse(name: string): Observable<any>{
    return this.httpClient.delete<any>(this.SERVER_ADDRESS+"/"+name, {headers: this.header});
  }
}