import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Group } from '../model/group';

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  private SERVER_ADDRESS: string  = "https://localhost:7129/Group";
  private header: HttpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private httpClient: HttpClient) { }
  createGroup(group: Group): Observable<any>{
    return this.httpClient.post<any>(this.SERVER_ADDRESS, group, {headers:this.header});
  }
  getGroups(name: string): Observable<Group[]>{
    return this.httpClient.get<Group[]>(this.SERVER_ADDRESS+"/"+name, {headers: this.header});
  }
  updateGroup(name: string, group: Group): Observable<any>{
    return this.httpClient.put<any>(this.SERVER_ADDRESS+"/"+name, group, {headers: this.header});
  }
  deleteGroup(name: string): Observable<any>{
    return this.httpClient.delete<any>(this.SERVER_ADDRESS+"/"+name, {headers: this.header});
  }
}
