import { TestBed } from '@angular/core/testing';

import { SpecializationService } from './specialization-service.service';

describe('SpecializationServiceService', () => {
  let service: SpecializationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpecializationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
