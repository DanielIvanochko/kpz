import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { PlatformMember } from 'src/app/model/platform-member';

@Injectable({
  providedIn: 'root'
})
export class PlatformMemberService {
  private SERVER_ADDRESS: string = 'https://localhost:7129/Member';
  private header: HttpHeaders;
  constructor(private httpClient: HttpClient) {
    this.header = new HttpHeaders().set('Content-Type', 'application/json');
  }
  createMember(member: PlatformMember): Observable<any> {
      return this.httpClient.post<any>(this.SERVER_ADDRESS, member, {headers: this.header});
  }
  findMembers(name: string): Observable<PlatformMember[]> {
    return this.httpClient.get<PlatformMember[]>(this.SERVER_ADDRESS+"/"+name,
      {headers:this.header});
  }
  deleteMember(fullName: string): Observable<any>{
    return this.httpClient.delete<any>(this.SERVER_ADDRESS+"/"+fullName, {headers: this.header});
  }
  updateMember(oldFullName: string, member: PlatformMember): Observable<any>{
    return this.httpClient.put<any>(this.SERVER_ADDRESS+"/"+oldFullName, member, {headers: this.header});
  }
}
