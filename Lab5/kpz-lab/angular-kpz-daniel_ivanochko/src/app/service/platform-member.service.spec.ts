import { TestBed } from '@angular/core/testing';

import { PlatformMemberService } from './platform-member.service';

describe('PlatformMemberService', () => {
  let service: PlatformMemberService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlatformMemberService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
