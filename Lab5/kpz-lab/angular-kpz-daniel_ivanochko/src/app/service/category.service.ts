import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CourseCategory } from '../model/course-category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private SERVER_ADDRESS: string = 'https://localhost:7129/Category';
  private header: HttpHeaders;
  constructor(private httpClient: HttpClient) {
    this.header = new HttpHeaders().set('Content-Type', 'application/json');
  }
  createCategory(spec : CourseCategory): Observable<any> {
    return this.httpClient.post<any>(this.SERVER_ADDRESS, spec, {headers: this.header});
  }
  updateCategory(oldName: string, spec : CourseCategory): Observable<any> {
    return this.httpClient.put<any>(this.SERVER_ADDRESS+"/"+oldName,spec, {headers: this.header});
  }
  deleteCategory(name: string): Observable<any>{
    return this.httpClient.delete<any>(this.SERVER_ADDRESS+"/"+name, {headers: this.header});
  }
  getCategories(name: string): Observable<CourseCategory[]>{
    return this.httpClient.get<CourseCategory[]>(this.SERVER_ADDRESS+"/"+name, {headers: this.header});
  }
}
