import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Specialization } from '../model/specialization';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpecializationService {
  private SERVER_ADDRESS: string = 'https://localhost:7129/Specialization';
  private header: HttpHeaders;
  constructor(private httpClient: HttpClient) {
    this.header = new HttpHeaders().set('Content-Type', 'application/json');
  }
  createSpecialization(spec : Specialization): Observable<any> {
    return this.httpClient.post<any>(this.SERVER_ADDRESS, spec, {headers: this.header});
  }
  updateSpecialization(oldName: string, spec : Specialization): Observable<any> {
    return this.httpClient.put<any>(this.SERVER_ADDRESS+"/"+oldName,spec, {headers: this.header});
  }
  deleteSpecialization(name: string): Observable<any>{
    return this.httpClient.delete<any>(this.SERVER_ADDRESS+"/"+name, {headers: this.header});
  }
  getSpecializations(name: string): Observable<Specialization[]>{
    return this.httpClient.get<Specialization[]>(this.SERVER_ADDRESS+"/"+name, {headers: this.header});
  }
}
