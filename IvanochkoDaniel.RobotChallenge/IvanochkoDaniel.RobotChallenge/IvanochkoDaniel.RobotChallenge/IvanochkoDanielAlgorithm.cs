﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IvanochkoDaniel.RobotChallenge
{
    public class IvanochkoDanielAlgorithm : IRobotAlgorithm
    {
        private static int minEnergyNeededForSpawning = 800;
        private static HashSet<Robot.Common.Robot> myRobots = new HashSet<Robot.Common.Robot>();
        private static int round = 0;
        public IvanochkoDanielAlgorithm() {
            Logger.OnLogRound += Logger_OnLogRound;
        }
        public static void setMyRobots(HashSet<Robot.Common.Robot> robots)
        {
            myRobots = robots;
        }
        public static HashSet<Robot.Common.Robot> getMyRobots()
        {
            return myRobots;
        }
        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            increaseRound();
        }
        public static void increaseRound()
        {
            round++;
        }
        public static int getCurrentRound()
        {
            return round;
        }
        RobotCommand IRobotAlgorithm.DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            
            var robot = robots[robotToMoveIndex];
            RobotCommand robotCommand = null;
            myRobots = (from rbt in robots where rbt.OwnerName=="Daniel Ivanochko" select rbt).ToHashSet();
            if (/*round > 10 && */robot.Energy >= minEnergyNeededForSpawning && myRobots.Count<100)
            {
                robotCommand = new CreateNewRobotCommand() { NewRobotEnergy = 400 };
            }
            else if (isRobotInCollectionPosition(robot, map))
            {
                robotCommand = new CollectEnergyCommand();
                MoveCommand testOtherPositionCommand = (MoveCommand)getMoveCommand(robot, robots, map, robot.Position);
                if(testOtherPositionCommand != null)
                {
                    double positionProfit = findProfitForPosition(testOtherPositionCommand.NewPosition, robot, robots, map);
                    var station = map.Stations.Where(stn => stn.Position == robot.Position).FirstOrDefault();
                    if (positionProfit != 0 && positionProfit > station.RecoveryRate)
                    {
                        robotCommand = testOtherPositionCommand;
                    }
                }
                
            }
            else if (robot.Energy > 500 && isThereMyRobotNearCurrentRobot(robot) && isRobotInCollectionPosition(robot, map))
            {
                robotCommand = getMoveCommand(robot, robots, map, robot.Position);
            }
            else
            {
                robotCommand = getMoveCommand(robot, robots, map, null);
            }
            return robotCommand;
        }
        private double findProfitForPosition(Position position, Robot.Common.Robot robot, IList<Robot.Common.Robot> robots, Map map)
        {
            double profit = findProfitForStationPosition(position, robot, map);
            if (profit == 0)
            {
                profit = findProfitForRobotPosition(position, robots, robot);
            }
            return profit;
        }
        private double findProfitForStationPosition(Position stationPosition, Robot.Common.Robot robot, Map map)
        {
            var station = map.Stations.Where(stn=>stn.Position== stationPosition).FirstOrDefault();
            if (station != null)
            {
                return station.RecoveryRate - Math.Pow(DistanceFinder.findDistance(stationPosition, robot.Position), 2);
            }
            return 0;
        }
        private double findProfitForRobotPosition(Position position, IList<Robot.Common.Robot> robots, Robot.Common.Robot robot)
        {
            var enemy = robots.Where(rbt=>rbt.Position==position).FirstOrDefault();
            if (enemy != null)
            {
                return enemy.Energy * 0.05 - 50 - Math.Pow(DistanceFinder.findDistance(enemy.Position, robot.Position), 2);
            }
            return 0;
        }
        private RobotCommand getMoveCommand(Robot.Common.Robot robot, IList<Robot.Common.Robot> robots, Map map, Position positionToAvoid)
        {
            RobotCommand robotCommand = null;
            CorrectRobotStepsFinder correctRobotStepsFinder = new CorrectRobotStepsFinder(robot, map, robots, positionToAvoid);
            Position position = correctRobotStepsFinder.findNextPosition();
            if (position != null)
            {
                robotCommand = new MoveCommand() { NewPosition = position };
            }
            return robotCommand;
        }
        private bool isThereMyRobotNearCurrentRobot(Robot.Common.Robot robot)
        {
            foreach(Robot.Common.Robot rbt in myRobots)
            {
                int distance = DistanceFinder.findDistance(robot.Position, rbt.Position);
                if (distance < 5)
                {
                    return true;
                }
            }
            return false;
        }
        private bool isRobotInCollectionPosition(Robot.Common.Robot robot, Map map)
        {
            foreach(var station in map.Stations)
            {
                if(station.Position == robot.Position && station.Energy > 0)
                {
                    return true;
                }
            }
            return false;
        }
        public String Author
        {
            get { return "Daniel Ivanochko"; }
        }
    }
}