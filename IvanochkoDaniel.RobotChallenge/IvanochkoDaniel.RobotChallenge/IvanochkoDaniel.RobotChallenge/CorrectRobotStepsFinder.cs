﻿using Robot.Common;
using System.Collections.Generic;

namespace IvanochkoDaniel.RobotChallenge
{
    public class CorrectRobotStepsFinder
    {
        private static readonly int NUMBER_OF_ROUNDS = 51;
        private RadiusExpandingByStepsAlgorithm radiusExpandingByStepsAlgorithm;
        private Robot.Common.Robot robot;
        private Map map;
        private IList<Robot.Common.Robot> robots;
        private Position positionToAvoid;


        public CorrectRobotStepsFinder(Robot.Common.Robot robot, Map map, IList<Robot.Common.Robot> robots,
            Position positionToAvoid)
        {
            this.robot = robot;
            this.map = map;
            this.robots = robots;
            this.positionToAvoid = positionToAvoid;
        }

        public Position findNextPosition()
        {
            Position position = null;
            int round = IvanochkoDanielAlgorithm.getCurrentRound();
            for(int steps = 1; steps <= NUMBER_OF_ROUNDS - round; steps++)
            {
                radiusExpandingByStepsAlgorithm = new RadiusExpandingByStepsAlgorithm(steps, robot, map, robots, positionToAvoid);
                position = radiusExpandingByStepsAlgorithm.findPositionAccordingToSteps();
                if (position != null)
                {
                    break;
                }
            }
            return position;
        }
    }
}
