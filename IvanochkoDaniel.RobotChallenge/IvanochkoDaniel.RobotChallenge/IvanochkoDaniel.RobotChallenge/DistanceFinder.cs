﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IvanochkoDaniel.RobotChallenge
{
    public class DistanceFinder
    {
        public static int findDistance(Position first, Position second)
        {
            double distance = Math.Sqrt((Math.Pow(first.X-second.X, 2) + Math.Pow(first.Y-second.Y, 2)));
            if(distance - (int)distance >= 0.1)
            {
                distance = (int)distance + 1;
            }
            return (int)distance;
        }
    }
}
