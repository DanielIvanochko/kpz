﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;



namespace IvanochkoDaniel.RobotChallenge
{
    public class BeatingRobotsAlgorithm
    {
        private Robot.Common.Robot robot;
        private Map map;
        private IList<Robot.Common.Robot> robots;
        private int radius;
        private HashSet<Robot.Common.Robot> myRobots;
        private Position positionToAvoid;
        private int steps;

        public BeatingRobotsAlgorithm(Robot.Common.Robot robot, Map map, IList<Robot.Common.Robot> robots, int radius,
            Position positionToAvoid, int steps)
        {
            this.robot = robot;
            this.map = map;
            this.robots = robots;
            this.radius = radius;
            this.myRobots = IvanochkoDanielAlgorithm.getMyRobots();
            this.positionToAvoid = positionToAvoid;
            this.steps = steps;
        }
        public List<EnergyStation> getStationsInRadius()
        {
            List<EnergyStation> stations = new List<EnergyStation>();
            foreach(var station in map.Stations)
            {
                if(isStationInRadius(station, robot) && !isStationTakenByMyRobot(station) && station.Position!=positionToAvoid && isStationReachable(station))
                {
                    stations.Add(station);
                }
            }
            return stations;
        }
        public bool isStationTakenByMyRobot(EnergyStation station)
        {
            return myRobots.Any(rbt => rbt.Position == station.Position);/* || robotsAndStationPositions.Values.Any(pos => pos == station.Position);*/
        }
        private EnergyStation findOptimalStation()
        {
            List<EnergyStation> stationsInRadius = getStationsInRadius();
            stationsInRadius.Sort((x, y) =>
            {
                double xValue = x.RecoveryRate - Math.Pow(DistanceFinder.findDistance(x.Position, robot.Position), 2);
                double yValue = y.RecoveryRate - Math.Pow(DistanceFinder.findDistance(y.Position, robot.Position), 2);
       
                return xValue.CompareTo(yValue);
            });
            EnergyStation result = null;
            if(stationsInRadius.Count > 0)
            {
                result = stationsInRadius.Last();
            }
            return result;
        }
        private bool isStationReachable(EnergyStation station)
        {
            int distance = DistanceFinder.findDistance(robot.Position, station.Position);
            int energyForOneStep = robot.Energy / steps;
            double distanceTakenByOneStep = Math.Sqrt(energyForOneStep);
            if(distanceTakenByOneStep - (int)distanceTakenByOneStep > 0.1)
            {
                distanceTakenByOneStep = (int)distanceTakenByOneStep + 1;
            }
            int distanceOfPossibleReachWithSuchSteps = (int)Math.Sqrt(energyForOneStep) * steps;
            double numberOfStepsNeededToReachStation = distance / distanceTakenByOneStep;
            if(numberOfStepsNeededToReachStation - (int)numberOfStepsNeededToReachStation > 0.1)
            {
                numberOfStepsNeededToReachStation = (int)numberOfStepsNeededToReachStation + 1;
            }
            bool result = false;
            if (distanceOfPossibleReachWithSuchSteps >= distance)
            {
                if (isEnemyRobotOnStation(station))
                {
                    result = robot.Energy - energyForOneStep * numberOfStepsNeededToReachStation - 50 < 0.001;
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }
        private bool isEnemyRobotOnStation(EnergyStation station)
        {
            if (robots != null)
            {
                return robots.Where(rbt => rbt.Position == station.Position).Any();
            }
            return false;
        }
        private Robot.Common.Robot findOptimalRobot()
        {
            List<Robot.Common.Robot> robotsInRadius = getRobotsInRadius();
            robotsInRadius.Sort((x, y) => (x.Energy*0.05-50-Math.Pow(DistanceFinder.findDistance(x.Position, robot.Position),2)).CompareTo((y.Energy*0.05-50-
                Math.Pow(DistanceFinder.findDistance(y.Position, robot.Position), 2))));
            Robot.Common.Robot result = null;
            if (robotsInRadius.Count > 0)
            {
                result = robotsInRadius.Last();
            }
            return result;
        }
        public Position findOptimalPosition()
        {
            EnergyStation optimalStation = findOptimalStation();
            Robot.Common.Robot optimalRobot = findOptimalRobot();

            double stationProfit = 0;
            double robotProfit = 0;
            if (optimalStation != null)
            {
                stationProfit = optimalStation.RecoveryRate - Math.Pow(DistanceFinder.findDistance(robot.Position, optimalStation.Position), 2);
            }
            if (optimalRobot != null)
            {
                robotProfit = optimalRobot.Energy * 0.05 - 50 - Math.Pow(DistanceFinder.findDistance(robot.Position, optimalRobot.Position), 2);
            }
            if(optimalStation == null && optimalRobot != null)
            {
                return optimalRobot.Position;
            }else if(optimalRobot == null && optimalStation != null)
            {
                return optimalStation.Position;
            }
            else if(optimalStation != null && optimalRobot != null)
            {
                bool isStationBetter = stationProfit.CompareTo(robotProfit) > 0;
                return isStationBetter ? optimalStation.Position : optimalRobot.Position;
            }
            return null;
        }
        public List<Robot.Common.Robot> getRobotsInRadius()
        {
            List<Robot.Common.Robot> robotsInRadius= new List<Robot.Common.Robot>();
            foreach (var rbt in robots)
            {
                if(rbt.OwnerName!="Daniel Ivanochko"&& isRobotInRadius(rbt, robot))
                {
                    robotsInRadius.Add(rbt);
                }
            }
            return robotsInRadius;
        }
        public bool isRobotInRadius(Robot.Common.Robot robot, Robot.Common.Robot other)
        {
            int distance = DistanceFinder.findDistance(other.Position, robot.Position);
            return distance <= getRadius();
        }
        public bool isStationInRadius(EnergyStation station, Robot.Common.Robot robot)
        { 
            int distance = DistanceFinder.findDistance(station.Position, robot.Position);
            return distance <= getRadius();
        }
        public void setRadius(int radius)
        {
            this.radius = radius;
        }
        public int getRadius()
        {
            return radius;
        }
    }
}
