﻿using Robot.Common;
using System;
using System.Collections.Generic;

namespace IvanochkoDaniel.RobotChallenge
{
    public class RadiusExpandingByStepsAlgorithm
    {
        private int steps;
        private Robot.Common.Robot robot;
        private Map map;
        private IList<Robot.Common.Robot> robots;
        private Position positionToAvoid;

        public RadiusExpandingByStepsAlgorithm(int steps, Robot.Common.Robot robot, Map map, IList<Robot.Common.Robot> robots,
            Position positionToAvoid)
        {
            this.steps = steps;
            this.robot = robot;
            this.map = map;
            this.robots = robots;
            this.positionToAvoid = positionToAvoid;
        }

        public Position findPositionAccordingToSteps()
        {
            Position position = null;
            int radius = (int)Math.Sqrt(robot.Energy);
            if (steps > 1)
            {
                radius = findRadiusAccordingToSteps();
            }
            BeatingRobotsAlgorithm beatingRobotsAlgorithm = new BeatingRobotsAlgorithm(robot, map, robots, radius, positionToAvoid, steps);
            position = beatingRobotsAlgorithm.findOptimalPosition();
            if (steps > 1)
            {
                position = findNextPositionAccordingToSteps(robot.Position, position, steps);
                if (position != null)
                {
                    /*I know that this 'if' sucks, but without it I have null ref exception*/
                    if (DistanceFinder.findDistance(position, robot.Position) > (int)Math.Sqrt(robot.Energy))
                    {
                        position = null;
                    }
                }
            }
            
            return position;
        }
        private Position findNextPositionAccordingToSteps(Position robotPosition, Position optimalPosition, int steps)
        {
            Position result = null;
            if(optimalPosition != null)
            {
                double lambda = 1.0 / steps;
                int x = (int)((robotPosition.X + (lambda) * optimalPosition.X) / (double)(1 + lambda));
                int y = (int)((robotPosition.Y + (lambda) * optimalPosition.Y) / (double)(1 + lambda));
                result = new Position(x, y);
            }
            return result;
        }
        private int findRadiusAccordingToSteps()
        {
            int radius = 0;
            int energyForOneStep = robot.Energy / steps;
            int distanceForOneStep = (int)Math.Sqrt(energyForOneStep);
            for(int i = 0; i < steps; i++)
            {
                radius += distanceForOneStep;
            }
            return radius;
        }
    }
}
