﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System;
using System.Collections.Generic;

namespace IvanochkoDaniel.RobotChallenge.Test
{
    [TestClass]
    public class UnitTest1
    {
        private static readonly int ROUNDS_NEEDED_FOR_ROBOT_CREATION = 16;
        [TestMethod]
        public void testDistanceFinder()
        {
            Position first = new Position(1,1);
            Position position = new Position(1,10);
            int distance = DistanceFinder.findDistance(first, position);
            Assert.AreEqual(9, distance);
        }
        [TestMethod]
        public void testDividingTheWayToStation()
        {
            Map map = new Map();
            map.Stations = new List<EnergyStation>()
            {
                new EnergyStation(){Position = new Position(16,2)}
            };
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot(){Energy=100, Position = new Position(1,1), OwnerName="Daniel Ivanochko"}
            };
            IRobotAlgorithm robotAlgorithm = new IvanochkoDanielAlgorithm();
            MoveCommand moveCommand = (MoveCommand)robotAlgorithm.DoStep(robots, 0, map);
            robots[0].Position = moveCommand.NewPosition;
            robots[0].Energy = 84;
            moveCommand = (MoveCommand)robotAlgorithm.DoStep(robots, 0, map);
            robots[0].Position = moveCommand.NewPosition;
            robots[0].Energy = 68;
            moveCommand = (MoveCommand)robotAlgorithm.DoStep(robots, 0, map);
            Assert.AreEqual(moveCommand.NewPosition, new Position(16, 2));
        }
        [TestMethod]
        public void testStationFinding()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation()
            {
                Energy = 100,
                Position = new Position(1, 10)
            });
            IRobotAlgorithm robotAlgorirhm = new IvanochkoDanielAlgorithm();
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Energy=100, Position=new Position(1,1), OwnerName="Daniel Ivanochko" });
            RobotCommand robotCommand = robotAlgorirhm.DoStep(robots, 0, map);
            Assert.IsNotNull(robotCommand);
            Assert.AreEqual(((MoveCommand)(robotCommand)).NewPosition, map.Stations[0].Position);
        }
        [TestMethod]
        public void testCollectEnergy()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation()
            {
                Energy = 100,
                Position = new Position(1, 1)
            });
            IRobotAlgorithm robotAlgorirhm = new IvanochkoDanielAlgorithm();
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Energy = 100, Position = new Position(1, 1), OwnerName = "Daniel Ivanochko" });
            RobotCommand robotCommand = robotAlgorirhm.DoStep(robots, 0, map);
            Assert.IsNotNull(robotCommand);
            Assert.IsTrue(robotCommand is CollectEnergyCommand);
        }
        [TestMethod]
        public void testRobotFighting()
        {
            Map map = new Map();
            IRobotAlgorithm robotAlgorirhm = new IvanochkoDanielAlgorithm();
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Energy = 100, Position = new Position(1, 1), OwnerName = "Daniel Ivanochko" });
            robots.Add(new Robot.Common.Robot() { Energy = 150, Position = new Position(1, 10), OwnerName = "Another Robot" });
            Position enemyPosition = robots[1].Position;
            IRobotAlgorithm algorithm = new IvanochkoDanielAlgorithm();
            RobotCommand robotCommand = algorithm.DoStep(robots, 0, map);
            Assert.IsNotNull(robotCommand);
            Assert.AreEqual(((MoveCommand)(robotCommand)).NewPosition, enemyPosition);
        }
        [TestMethod]
        public void testCreateRobot()
        {
            for(int i = 0;i < ROUNDS_NEEDED_FOR_ROBOT_CREATION; i++)
            {
                IvanochkoDanielAlgorithm.increaseRound();
            }
            Map map = new Map();
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 1000 } };
            IRobotAlgorithm robotAlgorithm = new IvanochkoDanielAlgorithm();
            RobotCommand robotCommand = robotAlgorithm.DoStep(robots, 0, map);
            Assert.IsNotNull(robotCommand);
            Assert.IsTrue(robotCommand is CreateNewRobotCommand);
        }
        [TestMethod]
        public void testGetStationsInRadius()
        {
            Map map = new Map();
            List<EnergyStation> stations = new List<EnergyStation>()
            {
                new EnergyStation() { Energy = 100, Position=new Position(1,5) },
                new EnergyStation(){ Energy = 100, Position= new Position(-1,4) },
                new EnergyStation(){ Energy = 100,Position=new Position(11,20) },
            };
            map.Stations = stations;
            Robot.Common.Robot robot = new Robot.Common.Robot() { Position = new Position(0,0), Energy = 100};
            int radius = 10;
            HashSet<Robot.Common.Robot> myRobots = new HashSet<Robot.Common.Robot>();
            myRobots.Add(robot);
            IvanochkoDanielAlgorithm.setMyRobots(myRobots);
            BeatingRobotsAlgorithm beatingRobotsAlgorithm = new BeatingRobotsAlgorithm(robot, map, null, radius, null, 1);
            List<EnergyStation> stationsInRadius = beatingRobotsAlgorithm.getStationsInRadius();
            Assert.IsTrue(stationsInRadius.Count == 2);
        }
        [TestMethod]
        public void testGetRobotsInRadius()
        {
            Map map = new Map();
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() { Energy = 100, Position=new Position(1,5) },
                new Robot.Common.Robot() { Energy = 100, Position= new Position(-1,4) },
                new Robot.Common.Robot() { Energy = 100,Position=new Position(11,20) },
            };
            Robot.Common.Robot robot = new Robot.Common.Robot() { Position = new Position(0, 0), Energy = 100, OwnerName="Daniel Ivanochko" };
            int radius = 10;
            BeatingRobotsAlgorithm beatingRobotsAlgorithm = new BeatingRobotsAlgorithm(robot, map, robots, radius, null, -1);
            var robotsInRadius = beatingRobotsAlgorithm.getRobotsInRadius();
            Assert.IsTrue(robotsInRadius.Count == 2);
        }
        [TestMethod]
        public void isStationTakenByMyRobot()
        {
            EnergyStation station = new EnergyStation() { Position = new Position(1, 1) };
            HashSet<Robot.Common.Robot> myRobots = new HashSet<Robot.Common.Robot>()
            {
                new Robot.Common.Robot() {Position = new Position(1, 1),}
            };
            IvanochkoDanielAlgorithm.setMyRobots(myRobots);
            BeatingRobotsAlgorithm beatingRobotsAlgorirhm = new BeatingRobotsAlgorithm(null, null, null, 10, null, -1);
            Assert.IsTrue(beatingRobotsAlgorirhm.isStationTakenByMyRobot(station));
        }
        [TestMethod]
        public void isRobotInRadius()
        {
            Robot.Common.Robot robot = new Robot.Common.Robot() { Position = new Position(0, 0) };
            Robot.Common.Robot secondRobot = new Robot.Common.Robot() { Position = new Position(5, 5) };
            BeatingRobotsAlgorithm beatingRobotsAlgorithm = new BeatingRobotsAlgorithm(null, null, null, 0, null, -1);
            beatingRobotsAlgorithm.setRadius(10);
            Assert.IsTrue(beatingRobotsAlgorithm.isRobotInRadius(robot, secondRobot));
        }
        [TestMethod]
        public void isStationInRadius()
        {
            Robot.Common.Robot robot = new Robot.Common.Robot() { Position = new Position(0, 0) };
            EnergyStation station = new EnergyStation() { Position = new Position(5, 5) };
            BeatingRobotsAlgorithm beatingRobotsAlgorithm = new BeatingRobotsAlgorithm(null, null, null, 0, null, -1);
            beatingRobotsAlgorithm.setRadius(10);
            Assert.IsTrue(beatingRobotsAlgorithm.isStationInRadius(station, robot));
        }
        [TestMethod]
        public void isStationInRadiusNegative()
        {
            Robot.Common.Robot robot = new Robot.Common.Robot() { Position = new Position(0, 0) };
            EnergyStation station = new EnergyStation() { Position = new Position(11, 11) };
            BeatingRobotsAlgorithm beatingRobotsAlgorithm = new BeatingRobotsAlgorithm(null, null, null, 0, null, -1);
            beatingRobotsAlgorithm.setRadius(10);
            Assert.IsFalse(beatingRobotsAlgorithm.isStationInRadius(station, robot));
        }
    }
}
