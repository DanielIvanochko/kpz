﻿using AutoMapper;
using DistanceLearningPlatform.CourseCreation.Model;
using DistanceLearningPlatform.CourseCreation.ModelsMapper;
using DistanceLearningPlatform.CourseCreation.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace DistanceLearningPlatform.CourseCreation
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static CourseOperationsModel courseOperationsModel;
        private static CourseOperationsViewModel courseOperationsViewModel;
        private static CourseEntitiesMapper courseEntitiesMapper;
        private static IMapper mapper;
        static App() {
            courseOperationsModel = new CourseOperationsModel();
            courseEntitiesMapper = new CourseEntitiesMapper();
            mapper = courseEntitiesMapper.Mapper;
            courseOperationsViewModel = mapper.Map<CourseOperationsViewModel>(courseOperationsModel);
        }
        public App()
        {

        }
        public static CourseOperationsModel CourseOperationsModel { get => courseOperationsModel; set=>courseOperationsModel = value ;}
        public static CourseOperationsViewModel CourseOperationsViewModel { get => courseOperationsViewModel; set => courseOperationsViewModel = value; }
        public static IMapper Mapper { get => mapper; set => mapper = value; }
    }
}
