﻿using DistanceLearningPlatform.CourseCreation.Model.Course;
using DistanceLearningPlatform.CourseCreation.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;
using System.Windows.Navigation;

namespace DistanceLearningPlatform.CourseCreation.Model
{
    public class CourseOperationsModel
    {
        private User user;
        private PlatformMember member;
        private List<Course.Course> courses;
        private List<Group> groups;
        private List<Specialization> specializations;
        private List<PlatformMember> platformMembers;
        private List<Role> roles;
        private List<CourseCategory> categories;

        public CourseOperationsModel() {
            user = new User("user", "password", new Role("ADMIN"));
            member = new PlatformMember("hello", "meow");
            platformMembers = new List<PlatformMember> { member };
            categories = new List<CourseCategory> { new CourseCategory("Bakalavrat"), new CourseCategory("Magistratura")};
            specializations = new List<Specialization> { new Specialization("PZ"), new Specialization("KN") };
            groups = new List<Group> { new Group("PZ-32", new List<PlatformMember>(), specializations[0] )};
            courses = new List<Course.Course>() { new Course.Course("KPZ", "WPF", categories[0], specializations[0], new List<PlatformMember>()) };
        }

        public CourseOperationsModel(User user,
            PlatformMember member, List<Course.Course> courses,
            List<Group> groups, List<Specialization> specializations,
            List<PlatformMember> platformMembers,
            List<Role> roles,
            List<CourseCategory> categories)
        {
            this.user = user;
            this.member = member;
            this.courses = courses;
            this.groups = groups;
            this.specializations = specializations;
            this.platformMembers = platformMembers;
            this.roles = roles;
            this.categories = categories;
        }

        public User User { get => user; set => user = value; }
        public PlatformMember Member { get => member; set => member = value; }
        public List<Course.Course> Courses { get => courses; set => courses = value; }
        public List<Group> Groups { get => groups; set => groups = value; }
        public List<Specialization> Specializations { get => specializations; set => specializations = value; }
        public List<PlatformMember> PlatformMembers { get => platformMembers; set => platformMembers = value; }
        public List<Role> Roles { get => roles; set => roles = value; }
        public List<CourseCategory> Categories { get => categories; set => categories = value; }

        public bool AddCourse(Course.Course course)
        {
            var courseCheckerCollection = (from crs in courses where crs.Name == course.Name select crs).ToList<Course.Course>();
            bool result = false;
            if (courseCheckerCollection.Count == 0)
            {
                courses.Add(course);
                result = true;
            }
            return result;
        }
        public bool AddGroup(Group group)
        {
            var groupFound = groups.FirstOrDefault(g => g.Name == group.Name) != null;
            bool result = false;
            if (!groupFound)
            {
                groups.Add(group);
                result = true;
            }
            return result;
        }
        public bool AddMember(PlatformMember member)
        {
            String name = member.FirstName + " " + member.LastName;
            var memberFound = platformMembers.FirstOrDefault(m => (m.FirstName + m.LastName) == name) != null;
            bool result = false;
            if (!memberFound)
            {
                platformMembers.Add(member);
                result = true;
            }
            return result;
        }
        public bool AddCategory(CourseCategory category)
        {
            var categoryFound = categories.FirstOrDefault(c => c.Name == category.Name) != null;
            bool result = false;
            if (!categoryFound)
            {
                categories.Add(category);
                result = true;
            }
            return result;
        }
        public bool AddSpecialization(Specialization specialization)
        {
            var specializationFound = specializations.FirstOrDefault(s => s.Name == specialization.Name) != null;
            bool result = false;
            if (!specializationFound)
            {
                specializations.Add(specialization);
                result = true;
            }
            return result;
        }
        
        public List<Course.Course> FindCoursesWhereNameContains(String value)
        {
            List<Course.Course> coursesFound = (from course in courses where course.Name.Contains(value) select course).ToList();
            return coursesFound;
        }
        public List<CourseCategory> FindCategoriesWhereNameContains(String value)
        {
            List<CourseCategory> categoriesFound = (from category in categories where category.Name.Contains(value) select category).ToList();
            return categoriesFound;
        }
        public List<Specialization> FindSpecializationsWhereNameContains(String value)
        {
            List<Specialization> specializationsFound = (from specialization in specializations where specialization.Name.Contains(value) select specialization).ToList();
            return specializationsFound;
        }
        public List<PlatformMember> FindPlatformMembersWhereNameContains(String value)
        {
            List<PlatformMember> membersFound = (from member in platformMembers where (member.FirstName + member.LastName).Contains(value) select member).ToList();
            return membersFound;
        }
        public List<Group> FindGroupsWhereNameContains(String value)
        {
            List<Group> groupsFound = (from g in groups where g.Name.Contains(value) select g).ToList();
            return groupsFound;
        }
        public bool DeleteCourse(Course.Course course)
        {
            CourseEntityDeletion courseEntityDeletion = new CourseDeletion(Courses, course);
            return courseEntityDeletion.DeleteCourseEntity();
        }
        public bool DeleteCategory(CourseCategory category)
        {
            CourseEntityDeletion courseEntityDeletion = new CategoryDeletion(Categories, category);
            return courseEntityDeletion.DeleteCourseEntity();
        }
        public bool DeleteSpecialization(Specialization specialization)
        {
            CourseEntityDeletion courseEntityDeletion = new SpecializationDeletion(specialization, Specializations);
            return courseEntityDeletion.DeleteCourseEntity();
        }
        public bool DeleteMember(PlatformMember member)
        {
            CourseEntityDeletion courseEntityDeletion = new MemberDeletion(member, PlatformMembers);
            return courseEntityDeletion.DeleteCourseEntity();
        }
        public bool DeleteGroup(Group group)
        {
            CourseEntityDeletion courseEntityDeletion = new GroupDeletion(Groups, group);
            return courseEntityDeletion.DeleteCourseEntity();
        }
        public bool UpdateCourse(string oldCourseName, Course.Course course)
        {
            bool result = false;
            var oldCourse = Courses.FirstOrDefault(crs => crs.Name == oldCourseName);
            if(oldCourse != null)
            {
                int index = courses.IndexOf(oldCourse);
                courses[index] = course;
                result = true;
            }
            return result;
        }
        public bool UpdateGroup(string oldGroupName, Group group)
        {
            bool result = false;
            var oldGroup = groups.FirstOrDefault(g => g.Name == oldGroupName);
            if(oldGroup != null)
            {
                int index = groups.IndexOf(oldGroup);
                groups[index] = group;
                result = true;
            }
            return result;
        }
        public bool UpdateMember(PlatformMember member, String oldName)
        {
            bool result = false;
            var oldMember = PlatformMembers.FirstOrDefault(m => (m.FirstName + " " + m.LastName) == oldName);
            if (oldMember != null)
            {
                int index = platformMembers.IndexOf(oldMember);
                platformMembers[index] = member;
                result = true;
            }
            return result;
        }
        public bool UpdateCategory(CourseCategory category, String oldName)
        {
            bool result = false;
            var oldCategory = Categories.FirstOrDefault(c => c.Name == oldName);
            if (oldCategory != null)
            {
                int index = categories.IndexOf(oldCategory);
                categories[index] = category;
                result = true;
            }
            return result;
        }
        public bool UpdateSpecialization(Specialization specialization, String oldName)
        {
            bool result = false;
            var oldSpecialization = Specializations.FirstOrDefault(s => s.Name == oldName);
            if (oldSpecialization != null)
            {
                int index = specializations.IndexOf(oldSpecialization);
                specializations[index] = specialization;
                result = true;
            }
            return result;
        }
    }
}
