﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.Model.Course
{
    public class CourseCategory
    {
        private String name;

        public CourseCategory(string name)
        {
            this.name = name;
        }

        public string Name { get => name; set => name = value; }
    }
}
