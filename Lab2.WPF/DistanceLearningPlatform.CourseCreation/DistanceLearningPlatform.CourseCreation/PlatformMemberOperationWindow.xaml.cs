﻿using DistanceLearningPlatform.CourseCreation.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DistanceLearningPlatform.CourseCreation
{
    /// <summary>
    /// Interaction logic for PlatformMemberOperationWindow.xaml
    /// </summary>
    public partial class PlatformMemberOperationWindow : Window
    {
        public PlatformMemberOperationWindow()
        {
            InitializeComponent();
            this.DataContext = new PlatformMemberOperationViewModel(App.CourseOperationsViewModel);
        }
        public PlatformMemberOperationWindow(PlatformMemberOperationViewModel platformMemberOperation)
        {
            InitializeComponent();
            this.DataContext = platformMemberOperation;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
