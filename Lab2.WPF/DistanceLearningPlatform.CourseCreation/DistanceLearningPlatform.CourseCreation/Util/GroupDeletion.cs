﻿using DistanceLearningPlatform.CourseCreation.Model.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.Util
{
    public class GroupDeletion : CourseEntityDeletion
    {
        private List<Group> groups;
        private Group groupForDeletion;

        public GroupDeletion(List<Group> groups, Group group)
        {
            this.groups = groups;
            this.groupForDeletion = group;
        }

        public bool DeleteCourseEntity()
        {
            var groupFound = groups.FirstOrDefault(g => g.Name == groupForDeletion.Name);
            bool result = false;
            if(groupFound != null)
            {
                result = groups.Remove(groupFound);
            }
            return result;
        }
    }
}
