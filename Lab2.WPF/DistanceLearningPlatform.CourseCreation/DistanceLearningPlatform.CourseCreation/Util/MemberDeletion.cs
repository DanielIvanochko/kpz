﻿using DistanceLearningPlatform.CourseCreation.Model.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.Util
{
    public class MemberDeletion : CourseEntityDeletion
    {
        private PlatformMember member;
        private List<PlatformMember> members;

        public MemberDeletion(PlatformMember member, List<PlatformMember> members)
        {
            this.member = member;
            this.members = members;
        }

        public bool DeleteCourseEntity()
        {
            String name = member.FirstName + " " + member.LastName;
            var memberFound = members.FirstOrDefault(m => (m.FirstName + " " + m.LastName).Contains(name));
            bool result = false;
            if(memberFound != null)
            {
                result = members.Remove(memberFound);
            }
            return result;
        }
    }
}
