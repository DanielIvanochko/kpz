﻿using DistanceLearningPlatform.CourseCreation.Model.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.Util
{
    public class SpecializationDeletion : CourseEntityDeletion
    {
        private Specialization specializationForDeletion;
        private List<Specialization> specializations;

        public SpecializationDeletion(Specialization specializationForDeletion, List<Specialization> specializations)
        {
            this.specializationForDeletion = specializationForDeletion;
            this.specializations = specializations;
        }
        public bool DeleteCourseEntity()
        {
            bool result = false;
            var specialization = specializations.FirstOrDefault(spc => spc.Name == specializationForDeletion.Name);
            if(specialization != null)
            {
                result = specializations.Remove(specialization);
            }
            return result;
        }
    }
}
