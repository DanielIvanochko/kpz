﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.RightsManagement;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class PlatformMemberOperationViewModel : ViewModelBase
    {
        private String windowTitle;
        private String firstName;
        private String lastName;
        private String oldName;
        private ICommand memberOperationCommand;
        private CourseOperationsViewModel courseOperationsViewModel;
        public PlatformMemberOperationViewModel(CourseOperationsViewModel courseOperationsViewModel)
        {
            windowTitle = "Create Platform Member";
            this.courseOperationsViewModel = courseOperationsViewModel;
            memberOperationCommand = new Command.Command(AddMember);
        }
        public PlatformMemberOperationViewModel(CourseOperationsViewModel courseOperationsViewModel, PlatformMemberViewModel platformMemberViewModel)
        {
            windowTitle = "Update Platform Member";
            this.courseOperationsViewModel = courseOperationsViewModel;
            FirstName = platformMemberViewModel.FirstName;
            LastName = platformMemberViewModel.LastName;
            oldName = platformMemberViewModel.Name;
            memberOperationCommand = new Command.Command(UpdateMember);
        }

        public string WindowTitle { get => windowTitle; set { windowTitle = value; OnPropertyChanged("WindowTitle"); } }
        public string FirstName { get => firstName; set { firstName = value; OnPropertyChanged("FirstName"); } }
        public string LastName { get => lastName; set { lastName = value; OnPropertyChanged("LastName"); } }
        public string OldName { get => oldName; set => oldName = value; }
        public ICommand MemberOperationCommand { get => memberOperationCommand; set { memberOperationCommand = value; OnPropertyChanged("MemberOperationCommand"); } }
        public void AddMember(object args)
        {
            PlatformMemberViewModel platformMemberViewModel = new PlatformMemberViewModel(FirstName, LastName);
            bool result = courseOperationsViewModel.AddMember(platformMemberViewModel);
            string message = result ? "Member created" : "Member WAS NOT created";
            MessageBox.Show(message);
        }
        public void UpdateMember(object args)
        {
            PlatformMemberViewModel platformMemberViewModel = new PlatformMemberViewModel(FirstName, LastName);
            bool result = courseOperationsViewModel.UpdateMember(platformMemberViewModel, OldName);
            string message = result ? "Member updated" : "Member WAS NOT updated";
            MessageBox.Show(message); 
        }
    }
}
