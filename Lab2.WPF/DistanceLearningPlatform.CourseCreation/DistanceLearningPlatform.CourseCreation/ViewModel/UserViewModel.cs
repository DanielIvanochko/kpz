﻿using DistanceLearningPlatform.CourseCreation.Model.Course;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class UserViewModel : ViewModelBase
    {
        private String login;
        private String password;
        private RoleViewModel role;

        public UserViewModel(string login, string password, RoleViewModel role)
        {
            this.login = login;
            this.password = password;
            this.role = role;
        }

        public string Login { get => login; set { login = value; OnPropertyChanged("Login"); } }
        public string Password { get => password; set { password = value; OnPropertyChanged("Password"); } }
        public RoleViewModel Role { get => role; set { role = value; OnPropertyChanged("Role"); } }
    }
}
