﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistanceLearningPlatform.CourseCreation.ViewModel
{
    public class PlatformMemberViewModel : ViewModelBase
    {
        private String firstName;
        private String lastName;

        public PlatformMemberViewModel(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public string FirstName { get => firstName; set { firstName = value; OnPropertyChanged("FirstName"); } }
        public string LastName { get => lastName; set { lastName = value; OnPropertyChanged("LastName"); } }

        public string Name { get => (FirstName + " " + LastName); }
    }
}
